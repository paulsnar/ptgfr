<?php

namespace Bot;

use Tgfr\TelegramObjects\Message;

class Hotwords
{
  static $instance;
  public static function getInstance($bot)
  {
    if (!self::$instance) {
      self::$instance = new Hotwords($bot);
    }

    return self::$instance;
  }

  protected $bot;
  public function __construct($bot) {
    $this->bot = $bot;
  }

  public function install($hotwordManager)
  {
    $hotwordManager->addHotword('hi', function ($trigger, Message $msg) {
      $msg->reply('Hello!');
    });
  }
}
