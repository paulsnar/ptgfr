<?php

namespace Bot;

use Tgfr\Bot as TgfrBot;

class Setup
{
  public static function install(TgfrBot $bot)
  {
    Commands::getInstance($bot)
      ->install($bot->getRouter()->getCommandManager());

    Hotwords::getInstance($bot)
      ->install($bot->getRouter()->getHotwordManager());

    Games::getInstance($bot)
      ->install($bot->getRouter()->getGameManager());
  }
}
