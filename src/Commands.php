<?php

namespace Bot;

use Tgfr\TelegramObjects\Message;

class Commands
{
  static $instance;
  public static function getInstance($bot)
  {
    if (!self::$instance) {
      self::$instance = new Commands($bot);
    }

    return self::$instance;
  }

  protected $bot;
  public function __construct($bot) {
    $this->bot = $bot;
  }

  public function install($commandManager)
  {
    $commandManager->handle('/ping', function ($cmd, $args, Message $msg) {
      $msg->respond('Pong!');
    });

    $commandManager->handle('/pong', [$this, 'handlePong']);
  }

  public function handlePong($cmd, $args, Message $msg)
  {
    $msg->respond('Ping?');
  }
}
