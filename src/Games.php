<?php

namespace Bot;

class Games
{
  static $instance;
  public static function getInstance($bot)
  {
    if (!self::$instance) {
      self::$instance = new Games($bot);
    }

    return self::$instance;
  }

  protected $bot;
  public function __construct($bot) {
    $this->bot = $bot;
  }

  public function install($gameManager)
  {
    // $gameManager->addGame('my_game', 'https://example.com');
  }
}
