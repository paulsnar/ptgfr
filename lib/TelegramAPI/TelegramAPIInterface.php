<?php

namespace Tgfr\TelegramAPI;

interface TelegramAPIInterface
{
  public function getMethod(string $method_name, array $arguments = [ ]);
  public function callMethod(string $method_name, array $arguments);
}
