<?php

namespace Tgfr\TelegramAPI;

/**
 * @codeCoverageIgnore
 */
class CurlTelegramAPI implements TelegramAPIInterface
{
  protected $dc;

  public function __construct(\Pimple\Container $dc)
  {
    $this->dc = $dc;
  }

  protected function createUrl(string $method_name)
  {
    $api_key = $this->dc['Bot']->getApiKey();
    return sprintf('https://api.telegram.org/bot%s/%s', $api_key, $method_name);
  }

  protected function performCurlCall(array $opts)
  {
    $ch = curl_init();

    $framework_version = $this->dc['Bot']->getFrameworkVersion();

    $overridable_opts = [
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_USERAGENT => sprintf('ptgfr/%s', $framework_version),
    ];

    curl_setopt_array($ch, ($opts + $overridable_opts));

    $response = curl_exec($ch);
    if ($response === false) {
      $e = new TelegramAPIException();
      $e->api_result = null;
      $e->setMessage(sprintf(
          'API could not be reached (cURL failure: %s)', curl_error($ch)));
      throw $e;
    }

    $data = json_decode($response, true);
    if (!$data['ok']) {
      $e = new TelegramAPIException();
      $e->api_result = $data;
      $e->setMessage('API rejected the method call');

      $this->dc['Log']->error(
        'Telegram API rejected a method call',
        [ 'response' => $data, 'input_opts' => $opts ]);
      throw $e;
    }

    curl_close($ch);
    return $data;
  }

  public function getMethod(string $method_name, array $arguments = [ ])
  {
    $url = $this->createUrl($method_name);
    if (count($arguments) > 0) {
      $url = $url . '?' . http_build_query($arguments);
    }
    return $this->performCurlCall([
      CURLOPT_URL => $url,
    ]);
  }

  public function callMethod(string $method_name, array $arguments)
  {
    return $this->performCurlCall([
      CURLOPT_URL => $this->createUrl($method_name),
      CURLOPT_POST => true,
      CURLOPT_POSTFIELDS => json_encode($arguments, JSON_FORCE_OBJECT),
      CURLOPT_HTTPHEADER => [ 'Content-Type: application/json; charset=utf-8' ],
    ]);
  }
}
