<?php

namespace Tgfr\TelegramAPI;

/**
 * @codeCoverageIgnore
 */
class TelegramAPIException extends \Exception
{
  public $api_result;
  public function setMessage(string $message)
  {
    $this->message = 'Telegram API exception: ' . $message;
  }
}
