<?php

namespace Tgfr\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface as In;
use Symfony\Component\Console\Output\OutputInterface as Out;
use Symfony\Component\Console\Style\SymfonyStyle;

use Tgfr\Bot;

class GetWebhookInfoCommand extends Command
{
  const NAME = 'telegram:get-webhook-info';

  /**
   * @codeCoverageIgnore
   */
  protected function configure()
  {
    $this
      ->setName(static::NAME)
      ->setDescription('Get information about the currently set webhook.')
      ->setHelp('(TODO)')
      ->addOption('apikey', null, InputOption::VALUE_REQUIRED,
        'The API key of your bot')
    ;
  }

  protected function execute(In $input, Out $output)
  {
    $io = new SymfonyStyle($input, $output);

    $api_key = $input->getOption('apikey');
    if (!$api_key) {
      $api_key = getenv('TELEGRAM_APIKEY');
    }
    if (!$api_key) {
      $api_key = $io->askHidden('Please enter your bot\'s API key now.');
    }

    $bot = new Bot($api_key);
    $dc = $bot->getDependencyContainer();
    $w = $dc['TelegramAPI']->invoke('getWebhookInfo', [ ], false);

    $io->success(
      'Here is the information about the current state of the webhook:');

    $lines = [ ];

    if ($w->getUrl() === '') {
      $lines[] = sprintf('<options=bold>%s</> <fg=yellow>%s</>',
        'Webhook enabled:',
        'no');
    } else {
      $lines[] = sprintf('<options=bold>%s</> <fg=green>%s</>',
        'Webhook enabled:',
        'yes');
      $lines[] = sprintf('<options=bold>%s</> %s',
        'Current URL:',
        $w->getUrl());
      $lines[] = sprintf('<options=bold>%s</> %s',
        'Has custom certificate:',
        $w->getHasCustomCertificate() ?
          '<fg=green>yes</>' :
          '<fg=yellow>no</>');
      $lines[] = sprintf('<options=bold>%s</> <fg=magenta>%d</>',
        'Pending updates:',
        $w->getPendingUpdateCount());
      if ($w->getMaxConnections()) {
        $lines[] = sprintf('<options=bold>%s</> <fg=magenta>%d</>',
          'Maximum simultaneous connection limit:',
          $w->getMaxConnections());
      }
      if ($w->getAllowedUpdates()) {
        $lines[] = sprintf('<options=bold>%s</> %s',
          'Enabled update types:',
          implode(', ', $w->getAllowedUpdates()));
      }
      if ($w->getLastErrorDate()) {
        $lines[] = sprintf('<options=bold,fg=yellow>%s</> %s',
          'Last error encountered at:',
          $w->getLastErrorDate()->format(\DateTime::RFC2822));
      }
      if ($w->getLastErrorMessage()) {
        $lines[] = sprintf('<options=bold,fg=yellow>%s</> %s',
          'Last error message:',
          $w->getLastErrorMessage());
      }
    }

    $output->writeln($lines);
  }
}
