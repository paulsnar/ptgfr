<?php

namespace Tgfr\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface as In;
use Symfony\Component\Console\Output\OutputInterface as Out;
use Symfony\Component\Console\Style\SymfonyStyle;

use Tgfr\Bot;

class DeleteWebhookCommand extends Command
{
  const NAME = 'telegram:delete-webhook';

  /**
   * @codeCoverageIgnore
   */
  protected function configure()
  {
    $this
      ->setName(static::NAME)
      ->setDescription('Remove Telegram webhook.')
      ->setHelp('(TODO)')
      ->addOption('apikey', null, InputOption::VALUE_REQUIRED,
        'The API key of your bot')
    ;
  }

  protected function execute(In $input, Out $output)
  {
    $io = new SymfonyStyle($input, $output);

    $api_key = $input->getOption('apikey');
    if (!$api_key) {
      $api_key = getenv('TELEGRAM_APIKEY');
    }
    if (!$api_key) {
      $api_key = $io->askHidden('Please enter your bot\'s API key now.');
    }

    $bot = new Bot($api_key);
    $dc = $bot->getDependencyContainer();
    $resp = $dc['TelegramAPI']->invoke('deleteWebhook', [ ], false);

    if ($resp) {
      $io->success('Webhook was deleted successfully.');
    }
  }
}
