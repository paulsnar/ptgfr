<?php

namespace Tgfr\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface as In;
use Symfony\Component\Console\Output\OutputInterface as Out;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Style\SymfonyStyle;

use Tgfr\Bot;
use Bot\Setup;

class RunPollerCommand extends Command
{
  const NAME = 'tgfr:run-poller';
  /**
   * @codeCoverageIgnore
   */
  protected function configure()
  {
    $this
      ->setName(static::NAME)
      ->setDescription('Run long polling instead of webhooks.')
      ->setHelp('(TODO)')
      ->addOption('apikey', null, InputOption::VALUE_REQUIRED,
        'The API key of your bot')
      ->addOption('interval', 'i', InputOption::VALUE_REQUIRED,
        'Polling interval in seconds')
    ;
  }

  protected function execute(In $input, Out $output)
  {
    $io = new SymfonyStyle($input, $output);

    $api_key = $input->getOption('apikey');
    if (!$api_key) {
      $api_key = getenv('TELEGRAM_APIKEY');
    }
    if (!$api_key) {
      $api_key = $io->askHidden('Please enter your bot\'s API key now.');
    }

    $io->note('Launching poller.');

    global $bot;
    $bot = new Bot($api_key);

    if ($output->getVerbosity() >= Out::VERBOSITY_DEBUG) {
      $bot->getDependencyContainer()['Log']->pushHandler(
        new \Monolog\Handler\StreamHandler('php://stderr',
          \Monolog\Logger::DEBUG));
      $progress = null;
    } else {
      $progress = new ProgressBar($output);
    }

    Setup::install($bot);

    // We must override these settings since otherwise the handling logic might
    // get confused.
    $bot->getDependencyContainer()['config.auto_inline'] = false;
    $bot->getDependencyContainer()['config.early_response'] = false;

    $last_update = null;
    while (true) {
      if ($progress !== null) { $progress->advance(); }

      $opts = [
        'timeout' => 15,
      ];

      if ($input->getOption('interval')) {
        $opts['timeout'] = (int) $input->getOption('interval');
      }
      if ($last_update !== null) {
        $opts['offset'] = $last_update + 1;
      }

      $updates = $bot->getDependencyContainer()['TelegramAPI']
        ->invoke('getUpdates', $opts);

      foreach ($updates as $update) {
        if ($last_update === null || $update->getUpdateId() > $last_update) {
          $last_update = $update->getUpdateId();
        }

        $bot->handleUpdate($update);
      }
    }
  }
}
