<?php

namespace Tgfr\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface as In;
use Symfony\Component\Console\Output\OutputInterface as Out;
use Symfony\Component\Console\Style\SymfonyStyle;

use Tgfr\Bot;

class SetWebhookCommand extends Command
{
  const NAME = 'telegram:set-webhook';
  /**
   * @codeCoverageIgnore
   */
  protected function configure()
  {
    $this
      ->setName(static::NAME)
      ->setDescription('Set Telegram webhook to point to this installation.')
      ->setHelp('(TODO)')
      ->addArgument('url', InputArgument::REQUIRED,
        'URL pointing to your index.php')
      ->addOption('apikey', null, InputOption::VALUE_REQUIRED,
        'The API key of your bot')
    ;
  }

  protected function execute(In $input, Out $output)
  {
    $io = new SymfonyStyle($input, $output);

    $api_key = $input->getOption('apikey');
    if (!$api_key) {
      $api_key = getenv('TELEGRAM_APIKEY');
    }
    if (!$api_key) {
      $api_key = $io->askHidden('Please enter your bot\'s API key now.');
    }

    $url = $input->getArgument('url');

    $bot = new Bot($api_key);
    $dc = $bot->getDependencyContainer();
    $resp = $dc['TelegramAPI']->invoke('setWebhook', [ 'url' => $url ], false);

    if ($resp) {
      $io->success('Webhook was set to: ' . $url);
    }
  }
}
