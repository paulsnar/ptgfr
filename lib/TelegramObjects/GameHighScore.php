<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class GameHighScore extends AbstractObject
{
  private $position;
  private $user;
  private $score;

  protected $dc;

  public function __construct(
    int $position,
    User $user,
    int $score,
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->position = $position;
    $this->user = $user;
    $this->score = $score;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $init['user'] = User::fromJSON($init['user']);

    return (new GameHighScore(
      $init['position'],
      $init['user'],
      $init['score'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getPosition() { return $this->position; }
  public function getUser()     { return $this->user;     }
  public function getScore()    { return $this->score;    }
  // @codeCoverageIgnoreEnd
}
