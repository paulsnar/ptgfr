<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class Chat extends AbstractObject
{
  private $id;
  private $type;
  private $title;
  private $username;
  private $first_name;
  private $last_name;
  private $all_members_are_administrators;

  protected $dc;

  public function __construct(
    int $id,
    string $type,
    string $title = null, /* @Optional */
    string $username = null, /* @Optional */
    string $first_name = null, /* @Optional */
    string $last_name = null, /* @Optional */
    bool $all_members_are_administrators = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->id = $id;
    $this->type = $type;
    $this->title = $title;
    $this->username = $username;
    $this->first_name = $first_name;
    $this->last_name = $last_name;
    $this->all_members_are_administrators = $all_members_are_administrators;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['title', 'username', 'first_name', 'last_name',
      'all_members_are_administrators'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    return (new Chat(
      $init['id'],
      $init['type'],
      $init['title'],
      $init['username'],
      $init['first_name'],
      $init['last_name'],
      $init['all_members_are_administrators'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getId()         { return $this->id;         }
  public function getType()       { return $this->type;       }
  public function getTitle()      { return $this->title;      }
  public function getUsername()   { return $this->username;   }
  public function getFirst_name() { return $this->first_name; }
  public function getLast_name()  { return $this->last_name;  }
  public function getAllMembersAreAdministrators()
  {
    return $this->all_members_are_administrators;
  }
  // @codeCoverageIgnoreEnd

  public function leave()
  {
    return $this->dc['TelegramAPI']->invoke(
      'leaveChat', [ 'chat_id' => $this->id ]);
  }
}
