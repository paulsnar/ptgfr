<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class Sticker extends AbstractObject
{
  private $file_id;
  private $width;
  private $height;
  private $thumb;
  private $emoji;
  private $file_size;

  public function __construct(
    string $file_id,
    int $width,
    int $height,
    PhotoSize $thumb = null, /* @Optional */
    string $emoji = null, /* @Optional */
    int $file_size = null /* @Optional */
  )
  {
    $this->file_id = $file_id;
    $this->width = $width;
    $this->height = $height;
    $this->thumb = $thumb;
    $this->emoji = $emoji;
    $this->file_size = $file_size;
  }

  public static function fromJSON($item)
  {
    $init = /* clone */ $item;

    $optionals = ['thumb', 'emoji', 'file_size'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    if ($init['thumb'] !== null) {
      $init['thumb'] = PhotoSize::fromJSON($init['thumb']);
    }

    return (new Sticker(
      $init['file_id'],
      $init['width'],
      $init['height'],
      $init['thumb'],
      $init['emoji'],
      $init['file_size']
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getFileId()   { return $this->file_id;    }
  public function getWidth()    { return $this->width;      }
  public function getHeight()   { return $this->height;     }
  public function getThumb()    { return $this->thumb;      }
  public function getEmoji()    { return $this->emoji;      }
  public function getFileSize() { return $this->file_size;  }
  // @codeCoverageIgnoreEnd
}
