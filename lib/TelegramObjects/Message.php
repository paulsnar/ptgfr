<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;
use Tgfr\TelegramObjects\Auxilinary\MessageEntityArray;
use Tgfr\TelegramObjects\Auxilinary\PhotoSizeArray;

class Message extends AbstractObject
{
  private $message_id;
  private $from;
  private $date;
  private $chat;
  private $forward_from;
  private $forward_from_chat;
  private $forward_from_message_id;
  private $forward_date;
  private $reply_to_message;
  private $edit_date;
  private $text;
  private $entities;
  private $audio;
  private $document;
  private $game;
  private $photo;
  private $sticker;
  private $video;
  private $voice;
  private $caption;
  private $contact;
  private $location;
  private $venue;
  private $new_chat_member;
  private $left_chat_member;
  private $new_chat_title;
  private $new_chat_photo;
  private $delete_chat_photo;
  private $group_chat_created;
  private $supergroup_chat_created;
  private $channel_chat_created;
  private $migrate_to_chat_id;
  private $migrate_from_chat_id;
  private $pinned_message;

  protected $dc;

  public function __construct(
    int $message_id,
    User $from = null, /* @Optional */
    int $date = null,
    Chat $chat = null,
    User $forward_from = null, /* @Optional */
    Chat $forward_from_chat = null, /* @Optional */
    int $forward_from_message_id = null, /* @Optional */
    int $forward_date = null, /* @Optional */
    Message $reply_to_message = null, /* @Optional */
    int $edit_date = null, /* @Optional */
    string $text = null, /* @Optional */
    MessageEntityArray $entities = null, /* @Optional */
    Audio $audio = null, /* @Optional */
    Document $document = null, /* @Optional */
    Game $game = null, /* @Optional */
    PhotoSizeArray $photo = null, /* @Optional */
    Sticker $sticker = null, /* @Optional */
    Video $video = null, /* @Optional */
    Voice $voice = null, /* @Optional */
    string $caption = null, /* @Optional */
    Contact $contact = null, /* @Optional */
    Location $location = null, /* @Optional */
    Venue $venue = null, /* @Optional */
    User $new_chat_member = null, /* @Optional */
    User $left_chat_member = null, /* @Optional */
    string $new_chat_title = null, /* @Optional */
    PhotoSizeArray $new_chat_photo = null, /* @Optional */
    bool $delete_chat_photo = null, /* @Optional */
    bool $group_chat_created = null, /* @Optional */
    bool $supergroup_chat_created = null, /* @Optional */
    bool $channel_chat_created = null, /* @Optional */
    int $migrate_to_chat_id = null, /* @Optional */
    int $migrate_from_chat_id = null, /* @Optional */
    Message $pinned_message = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->message_id = $message_id;
    $this->from = $from;
    $this->date = new \DateTime('@' . $date);
    $this->chat = $chat;
    $this->forward_from = $forward_from;
    $this->forward_from_chat = $forward_from_chat;
    $this->forward_from_message_id = $forward_from_message_id;
    if ($forward_date !== null) {
      $this->forward_date = new \DateTime('@' . $forward_date);
    } else {
      $this->forward_date = null;
    }
    $this->reply_to_message = $reply_to_message;
    if ($edit_date !== null) {
      $this->edit_date = new \DateTime('@' . $edit_date);
    } else {
      $this->edit_date = null;
    }
    $this->text = $text;
    $this->entities = $entities;
    $this->audio = $audio;
    $this->document = $document;
    $this->game = $game;
    $this->photo = $photo;
    $this->sticker = $sticker;
    $this->video = $video;
    $this->voice = $voice;
    $this->caption = $caption;
    $this->contact = $contact;
    $this->location = $location;
    $this->venue = $venue;
    $this->new_chat_member = $new_chat_member;
    $this->left_chat_member = $left_chat_member;
    $this->new_chat_title = $new_chat_title;
    $this->new_chat_photo = $new_chat_photo;
    $this->delete_chat_photo = $delete_chat_photo;
    $this->group_chat_created = $group_chat_created;
    $this->supergroup_chat_created = $supergroup_chat_created;
    $this->channel_chat_created = $channel_chat_created;
    $this->migrate_to_chat_id = $migrate_to_chat_id;
    $this->migrate_from_chat_id = $migrate_from_chat_id;
    $this->pinned_message = $pinned_message;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['from', 'forward_from', 'forward_from_chat',
      'forward_from_message_id', 'forward_date', 'reply_to_message',
      'edit_date', 'text', 'entities', 'audio', 'document', 'game', 'photo',
      'sticker', 'video', 'voice', 'caption', 'contact', 'location', 'venue',
      'new_chat_member', 'left_chat_member', 'new_chat_title',
      'new_chat_photo', 'delete_chat_photo', 'group_chat_created',
      'supergroup_chat_created', 'channel_chat_created', 'migrate_to_chat_id',
      'migrate_from_chat_id', 'pinned_message'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    $user_fields = [
      'from',
      'forward_from',
      'new_chat_member',
      'left_chat_member',
    ];
    $chat_fields = [
      'chat',
      'forward_from_chat',
    ];
    $message_fields = [
      'reply_to_message',
      'pinned_message',
    ];
    $photosizearray_fields = [
      'photo',
      'new_chat_photo',
    ];

    foreach ($user_fields as $key) {
      if ($init[$key] !== null) {
        $init[$key] = User::fromJSON($init[$key], $dc);
      }
    }
    foreach ($chat_fields as $key) {
      if ($init[$key] !== null) {
        $init[$key] = Chat::fromJSON($init[$key], $dc);
      }
    }
    foreach ($message_fields as $key) {
      if ($init[$key] !== null) {
        $init[$key] = Message::fromJSON($init[$key], $dc);
      }
    }
    foreach ($photosizearray_fields as $key) {
      if ($init[$key] !== null) {
        $init[$key] = PhotoSizeArray::fromJSON($init[$key], $dc);
      }
    }

    if ($init['entities'] !== null) {
      $init['entities'] = MessageEntityArray::fromJSON($init['entities'], $dc);
    }
    if ($init['audio'] !== null) {
      $init['audio'] = Audio::fromJSON($init['audio'], $dc);
    }
    if ($init['document'] !== null) {
      $init['document'] = Document::fromJSON($init['document'], $dc);
    }
    if ($init['game'] !== null) {
      $init['game'] = Game::fromJSON($init['game'], $dc);
    }
    if ($init['sticker'] !== null) {
      $init['sticker'] = Sticker::fromJSON($init['sticker'], $dc);
    }
    if ($init['video'] !== null) {
      $init['video'] = Video::fromJSON($init['video'], $dc);
    }
    if ($init['voice'] !== null) {
      $init['voice'] = Voice::fromJSON($init['voice'], $dc);
    }
    if ($init['contact'] !== null) {
      $init['contact'] = Contact::fromJSON($init['contact'], $dc);
    }
    if ($init['location'] !== null) {
      $init['location'] = Location::fromJSON($init['location'], $dc);
    }
    if ($init['venue'] !== null) {
      $init['venue'] = Venue::fromJSON($init['venue'], $dc);
    }

    return (new Message(
      $init['message_id'],
      $init['from'],
      $init['date'],
      $init['chat'],
      $init['forward_from'],
      $init['forward_from_chat'],
      $init['forward_from_message_id'],
      $init['forward_date'],
      $init['reply_to_message'],
      $init['edit_date'],
      $init['text'],
      $init['entities'],
      $init['audio'],
      $init['document'],
      $init['game'],
      $init['photo'],
      $init['sticker'],
      $init['video'],
      $init['voice'],
      $init['caption'],
      $init['contact'],
      $init['location'],
      $init['venue'],
      $init['new_chat_member'],
      $init['left_chat_member'],
      $init['new_chat_title'],
      $init['new_chat_photo'],
      $init['delete_chat_photo'],
      $init['group_chat_created'],
      $init['supergroup_chat_created'],
      $init['channel_chat_created'],
      $init['migrate_to_chat_id'],
      $init['migrate_from_chat_id'],
      $init['pinned_message'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getMessageId()        { return $this->message_id;         }
  public function getFrom()             { return $this->from;               }
  public function getDate()             { return $this->date;               }
  public function getChat()             { return $this->chat;               }
  public function getForwardFrom()      { return $this->forward_from;       }
  public function getForwardFromChat()  { return $this->forward_from_chat;  }
  public function getForwardFromMessageId()
  {
    return $this->forward_from_message_id;
  }
  public function getForwardDate()      { return $this->forward_date;       }
  public function getReplyToMessage()   { return $this->reply_to_message;   }
  public function getEditDate()         { return $this->edit_date;          }
  public function getText()             { return $this->text;               }
  public function getEntities()         { return $this->entities;           }
  public function getAudio()            { return $this->audio;              }
  public function getDocument()         { return $this->document;           }
  public function getGame()             { return $this->game;               }
  public function getPhoto()            { return $this->photo;              }
  public function getSticker()          { return $this->sticker;            }
  public function getVideo()            { return $this->video;              }
  public function getVoice()            { return $this->voice;              }
  public function getCaption()          { return $this->caption;            }
  public function getContact()          { return $this->contact;            }
  public function getLocation()         { return $this->location;           }
  public function getVenue()            { return $this->venue;              }
  public function getNewChatMember()    { return $this->new_chat_member;    }
  public function getLeftChatMember()   { return $this->left_chat_member;   }
  public function getNewChatTitle()     { return $this->new_chat_title;     }
  public function getNewChatPhoto()     { return $this->new_chat_photo;     }
  public function getDeleteChatPhoto()  { return $this->delete_chat_photo;  }
  public function getGroupChatCreated() { return $this->group_chat_created; }
  public function getSupergroupChatCreated()
  {
    return $this->supergroup_chat_created;
  }
  public function getChannelChatCreated()
  {
    return $this->channel_chat_created;
  }
  public function getMigrateToChatId()  { return $this->migrate_to_chat_id; }
  public function getMigrateFromChatId()
  {
    return $this->migrate_from_chat_id;
  }
  public function getPinnedMessage()    { return $this->pinned_message;     }
  // @codeCoverageIgnoreEnd


  public function respond($response)
  {
    if (is_string($response)) {
      $response = [ 'text' => $response ];
    }

    return $this->dc['TelegramAPI']->invoke(
      'sendMessage', [ 'chat_id' => $this->chat->getId() ] + $response);
  }

  public function reply($response)
  {
    if (is_string($response)) {
      $response = [ 'text' => $response ];
    }

    return $this->dc['TelegramAPI']->invoke(
      'sendMessage',
      [ 'chat_id' => $this->chat->getId(),
        'reply_to_message_id' => $this->message_id ] + $response);
  }

  public function forwardTo($chat_id, array $options = [ ])
  {
    return $this->dc['TelegramAPI']->invoke(
      'forwardMessage',
      [ 'chat_id' => $chat_id,
        'from_chat_id' => $this->chat->getId(),
        'message_id' => $this->message_id ] + $options);
  }

  public function editText($new_text, array $options = [ ])
  {
    return $this->dc['TelegramAPI']->invoke(
      'editMessageText',
      [ 'chat_id' => $this->chat->getId(),
        'message_id' => $this->message_id,
        'text' => $new_text ] + $options);
  }

  public function editCaption($new_caption, array $options = [ ])
  {
    return $this->dc['TelegramAPI']->invoke(
      'editMessageCaption',
      [ 'chat_id' => $this->chat->getId(),
        'message_id' => $this->message_id,
        'caption' => $new_caption ] + $options);
  }

  public function editReplyMarkup($new_reply_markup, array $options = [ ])
  {
    return $this->dc['TelegramAPI']->invoke(
      'editMessageReplyMarkup',
      [ 'chat_id' => $this->chat->getId(),
        'message_id' => $this->message_id,
        'reply_markup' => $new_reply_markup ] + $options);
  }
}
