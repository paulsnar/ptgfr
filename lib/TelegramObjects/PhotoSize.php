<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class PhotoSize extends AbstractObject
{
  private $file_id;
  private $width;
  private $height;
  private $file_size;

  protected $dc;

  public function __construct(
    string $file_id,
    int $width,
    int $height,
    int $file_size = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->file_id = $file_id;
    $this->width = $width;
    $this->height = $height;
    $this->file_size = $file_size;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    if (!array_key_exists('file_size', $init)) {
      $init['file_size'] = null;
    }

    return (new PhotoSize(
      $init['file_id'],
      $init['width'],
      $init['height'],
      $init['file_size'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getFileId()   { return $this->file_id;    }
  public function getWidth()    { return $this->width;      }
  public function getHeight()   { return $this->height;     }
  public function getFileSize() { return $this->file_size;  }
  // @codeCoverageIgnoreEnd
}
