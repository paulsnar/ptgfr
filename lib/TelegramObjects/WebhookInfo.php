<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class WebhookInfo extends AbstractObject
{
  private $url;
  private $has_custom_certificate;
  private $pending_update_count;
  private $last_error_date;
  private $last_error_message;
  private $max_connections;
  private $allowed_updates;

  protected $dc;

  public function __construct(
    string $url,
    bool $has_custom_certificate,
    int $pending_update_count,
    int $last_error_date = null, /* @Optional */
    string $last_error_message = null, /* @Optional */
    int $max_connections = null, /* @Optional */
    array $allowed_updates = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->url = $url;
    $this->has_custom_certificate = $has_custom_certificate;
    $this->pending_update_count = $pending_update_count;
    if ($last_error_date !== null) {
      $this->last_error_date = new \DateTime('@' .$last_error_date);
    } else {
      $this->last_error_date = null;
    }
    $this->last_error_message = $last_error_message;
    $this->max_connections = $max_connections;
    $this->allowed_updates = $allowed_updates;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['last_error_date', 'last_error_message', 'max_connections',
      'allowed_updates'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    return (new WebhookInfo(
      $init['url'],
      $init['has_custom_certificate'],
      $init['pending_update_count'],
      $init['last_error_date'],
      $init['last_error_message'],
      $init['max_connections'],
      $init['allowed_updates'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getUrl()              { return $this->url;                }
  public function getHasCustomCertificate()
  {
    return $this->has_custom_certificate;
  }
  public function getPendingUpdateCount()
  {
    return $this->pending_update_count;
  }
  public function getLastErrorDate()    { return $this->last_error_date;    }
  public function getLastErrorMessage() { return $this->last_error_message; }
  public function getMaxConnections()   { return $this->max_connections;    }
  public function getAllowedUpdates()   { return $this->allowed_updates;    }
  // @codeCoverageIgnoreEnd
}
