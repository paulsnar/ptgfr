<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class Location extends AbstractObject
{
  private $longitude;
  private $latitude;

  protected $dc;

  public function __construct(
    float $longitude,
    float $latitude,
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->longitude = $longitude;
    $this->latitude = $latitude;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    return (new Location(
      $init['longitude'],
      $init['latitude'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getLongitude()  { return $this->longitude;  }
  public function getLatitude()   { return $this->latitude;   }
  // @codeCoverageIgnoreEnd
}
