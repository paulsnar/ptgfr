<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class ChatMember extends AbstractObject
{
  private $user;
  private $status;

  protected $dc;

  public function __construct(
    User $user,
    string $status,
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->user = $user;
    $this->status = $status;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $init['user'] = User::fromJSON($init['user']);

    return (new ChatMember(
      $init['user'],
      $init['status'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getUser()   { return $this->user;   }
  public function getStatus() { return $this->status; }
  // @codeCoverageIgnoreEnd
}
