<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class CallbackQuery extends AbstractObject
{
  private $id;
  private $from;
  private $message;
  private $inline_message_id;
  private $chat_instance;
  private $data;
  private $game_short_name;

  protected $dc;

  public function __construct(
    string $id,
    User $from,
    Message $message = null, /* @Optional */
    string $inline_message_id = null, /* @Optional */
    string $chat_instance = null,
    string $data = null, /* @Optional */
    string $game_short_name = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->id = $id;
    $this->from = $from;
    $this->message = $message;
    $this->inline_message_id = $inline_message_id;
    $this->chat_instance = $chat_instance;
    $this->data = $data;
    $this->game_short_name = $game_short_name;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['message', 'inline_message_id', 'data', 'game_short_name'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    $init['from'] = User::fromJSON($init['from'], $dc);

    if ($init['message'] !== null) {
      $init['message'] = Message::fromJSON($init['message'], $dc);
    }

    return (new CallbackQuery(
      $init['id'],
      $init['from'],
      $init['message'],
      $init['inline_message_id'],
      $init['chat_instance'],
      $init['data'],
      $init['game_short_name'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getId()               { return $this->id;                 }
  public function getFrom()             { return $this->from;               }
  public function getMessage()          { return $this->message;            }
  public function getInlineMessageId()  { return $this->inline_message_id;  }
  public function getChatInstance()     { return $this->chat_instance;      }
  public function getData()             { return $this->data;               }
  public function getGameShortName()    { return $this->game_short_name;    }
  // @codeCoverageIgnoreEnd

  public function answer($response, array $options = [ ])
  {
    if (is_string($response)) {
      $response = [ 'text' => $response ];
    }

    $response = $response + $options;

    return $this->dc['TelegramAPI']->invoke('answerCallbackQuery',
      [ 'callback_query_id' => $this->id ] + $response);
  }
}
