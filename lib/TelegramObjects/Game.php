<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;
use Tgfr\TelegramObjects\Auxilinary\MessageEntityArray;
use Tgfr\TelegramObjects\Auxilinary\PhotoSizeArray;

class Game extends AbstractObject
{
  private $title;
  private $description;
  private $photo;
  private $text;
  private $text_entities;
  private $animation;

  protected $dc;

  public function __construct(
    string $title,
    string $description,
    PhotoSizeArray $photo,
    string $text = null, /* @Optional */
    MessageEntityArray $text_entities = null, /* @Optional */
    Animation $animation = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->title = $title;
    $this->description = $description;
    $this->photo = $photo;
    $this->text = $text;
    $this->text_entities = $text_entities;
    $this->animation = $animation;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['text', 'text_entities', 'animation'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    $init['photo'] = PhotoSizeArray::fromJSON($init['photo']);

    if ($init['text_entities'] !== null) {
      $init['text_entities'] =
        MessageEntityArray::fromJSON($init['text_entities']);
    }
    if ($init['animation'] !== null) {
      $init['animation'] = Animation::fromJSON($init['animation']);
    }

    return (new Game(
      $init['title'],
      $init['description'],
      $init['photo'],
      $init['text'],
      $init['text_entities'],
      $init['animation'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getTitle()        { return $this->title;          }
  public function getDescription()  { return $this->description;    }
  public function getPhoto()        { return $this->photo;          }
  public function getText()         { return $this->text;           }
  public function getTextEntities() { return $this->text_entities;  }
  public function getAnimation()    { return $this->animation;      }
  // @codeCoverageIgnoreEnd

  public function setScore(User $user, int $score, array $options = [ ])
  {
    return $this->dc['TelegramAPI']->invoke(
      'setGameScore',
      [ 'user_id' => $user->getId(),
        'score' => $score ] + $options);
  }
}
