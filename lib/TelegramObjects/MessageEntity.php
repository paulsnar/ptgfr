<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class MessageEntity extends AbstractObject
{
  private $type;
  private $offset;
  private $length;
  private $url;
  private $user;

  protected $dc;

  public function __construct(
    string $type,
    int $offset,
    int $length,
    string $url = null, /* @Optional */
    User $user = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->type = $type;
    $this->offset = $offset;
    $this->length = $length;
    $this->url = $url;
    $this->user = $user;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['url', 'user'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    if ($init['user'] !== null) {
      $init['user'] = User::fromJSON($init['user'], $dc);
    }

    return (new MessageEntity(
      $init['type'],
      $init['offset'],
      $init['length'],
      $init['url'],
      $init['user'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getType()   { return $this->type;   }
  public function getOffset() { return $this->offset; }
  public function getLength() { return $this->length; }
  public function getUrl()    { return $this->url;    }
  public function getUser()   { return $this->user;   }
  // @codeCoverageIgnoreEnd
}
