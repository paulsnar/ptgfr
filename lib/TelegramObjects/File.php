<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class File extends AbstractObject
{
  private $file_id;
  private $file_size;
  private $file_path;

  protected $dc;

  public function __construct(
    string $file_id,
    int $file_size = null, /* @Optional */
    string $file_path = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->file_id = $file_id;
    $this->file_size = $file_size;
    $this->file_path = $file_path;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['file_size', 'file_path'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    return (new File(
      $init['file_id'],
      $init['file_size'],
      $init['file_path'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getFileId()   { return $this->file_id;    }
  public function getFileSize() { return $this->file_size;  }
  public function getFilePath() { return $this->file_path;  }
  // @codeCoverageIgnoreEnd
}
