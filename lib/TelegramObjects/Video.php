<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class Video extends AbstractObject
{
  private $file_id;
  private $width;
  private $height;
  private $duration;
  private $thumb;
  private $mime_type;
  private $file_size;

  protected $dc;

  public function __construct(
    string $file_id,
    int $width,
    int $height,
    int $duration,
    PhotoSize $thumb = null, /* @Optional */
    string $mime_type = null, /* @Optional */
    int $file_size = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->file_id = $file_id;
    $this->width = $width;
    $this->height = $height;
    $this->duration = $duration;
    $this->thumb = $thumb;
    $this->mime_type = $mime_type;
    $this->file_size = $file_size;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['thumb', 'mime_type', 'file_size'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    if ($init['thumb'] !== null) {
      $init['thumb'] = PhotoSize::fromJSON($init['thumb'], $dc);
    }

    return (new Video(
      $init['file_id'],
      $init['width'],
      $init['height'],
      $init['duration'],
      $init['thumb'],
      $init['mime_type'],
      $init['file_size'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getFileId()   { return $this->file_id;    }
  public function getWidth()    { return $this->width;      }
  public function getHeight()   { return $this->height;     }
  public function getDuration() { return $this->duration;   }
  public function getThumb()    { return $this->thumb;      }
  public function getMimeType() { return $this->mime_type;  }
  public function getFileSize() { return $this->file_size;  }
  // @codeCoverageIgnoreEnd
}
