<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class Update extends AbstractObject
{
  private $update_id;
  private $message;
  private $edited_message;
  private $channel_post;
  private $edited_channel_post;
  private $inline_query;
  private $chosen_inline_result;
  private $callback_query;

  protected $dc;

  public function __construct(
    int $update_id,
    Message $message = null, /* @Optional */
    Message $edited_message = null, /* @Optional */
    Message $channel_post = null, /* @Optional */
    Message $edited_channel_post = null, /* @Optional */
    InlineQuery $inline_query = null, /* @Optional */
    ChosenInlineResult $chosen_inline_result = null, /* @Optional */
    CallbackQuery $callback_query = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->update_id = $update_id;
    $this->message = $message;
    $this->edited_message = $edited_message;
    $this->channel_post = $channel_post;
    $this->edited_channel_post = $edited_channel_post;
    $this->inline_query = $inline_query;
    $this->chosen_inline_result = $chosen_inline_result;
    $this->callback_query = $callback_query;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['message', 'edited_message', 'channel_post',
      'edited_channel_post', 'inline_query', 'chosen_inline_result',
      'callback_query'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    $message_keys = [
      'message',
      'edited_message',
      'channel_post',
      'edited_channel_post'
    ];

    foreach ($message_keys as $key) {
      if ($init[$key] !== null) {
        $init[$key] = Message::fromJSON($init[$key], $dc);
      }
    }

    if ($init['inline_query'] !== null) {
      $init['inline_query'] =
        InlineQuery::fromJSON($init['inline_query'], $dc);
    }
    if ($init['chosen_inline_result'] !== null) {
      $init['chosen_inline_result'] =
        ChosenInlineResult::fromJSON($init['chosen_inline_result'], $dc);
    }
    if ($init['callback_query'] !== null) {
      $init['callback_query'] =
        CallbackQuery::fromJSON($init['callback_query'], $dc);
    }

    return (new Update(
      $init['update_id'],
      $init['message'],
      $init['edited_message'],
      $init['channel_post'],
      $init['edited_channel_post'],
      $init['inline_query'],
      $init['chosen_inline_result'],
      $init['callback_query'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getUpdateId()       { return $this->update_id;        }
  public function getMessage()        { return $this->message;          }
  public function getEditedMessage()  { return $this->edited_message;   }
  public function getChannelPost()    { return $this->channel_post;     }
  public function getEditedChannelPost()
  {
    return $this->edited_channel_post;
  }
  public function getInlineQuery()    { return $this->inline_query;     }
  public function getChosenInlineResult()
  {
    return $this->chosen_inline_result;
  }
  public function getCallbackQuery()   { return $this->callback_query;  }
  // @codeCoverageIgnoreEnd
}
