<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class ChosenInlineResult extends AbstractObject
{
  private $result_id;
  private $from;
  private $location;
  private $inline_message_id;
  private $query;

  protected $dc;

  public function __construct(
    string $result_id,
    User $from,
    Location $location = null, /* @Optional */
    string $inline_message_id = null,
    string $query = null,
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->result_id = $result_id;
    $this->from = $from;
    $this->location = $location;
    $this->inline_message_id = $inline_message_id;
    $this->query = $query;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['location', 'inline_message_id'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    $init['from'] = User::fromJSON($init['from'], $dc);

    if ($init['location'] !== null) {
      $init['location'] = Location::fromJSON($init['location'], $dc);
    }

    return (new ChosenInlineResult(
      $init['result_id'],
      $init['from'],
      $init['location'],
      $init['inline_message_id'],
      $init['query'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getResult_id()          { return $this->result_id;          }
  public function getFrom()               { return $this->from;               }
  public function getLocation()           { return $this->location;           }
  public function getInline_message_id()  { return $this->inline_message_id;  }
  public function getQuery()              { return $this->query;              }
  // @codeCoverageIgnoreEnd
}
