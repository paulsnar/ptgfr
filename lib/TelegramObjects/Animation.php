<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class Animation extends AbstractObject
{
  private $file_id;
  private $thumb;
  private $file_name;
  private $mime_type;
  private $file_size;

  protected $dc;

  public function __construct(
    string $file_id,
    PhotoSize $thumb = null, /* @Optional */
    string $file_name = null, /* @Optional */
    string $mime_type = null, /* @Optional */
    int $file_size = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->file_id = $file_id;
    $this->thumb = $thumb;
    $this->file_name = $file_name;
    $this->mime_type = $mime_type;
    $this->file_size = $file_size;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['thumb', 'file_name', 'mime_type', 'file_size'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    if ($init['thumb'] !== null) {
      $init['thumb'] = PhotoSize::fromJSON($init['thumb']);
    }

    return (new Animation(
      $init['file_id'],
      $init['thumb'],
      $init['file_name'],
      $init['mime_type'],
      $init['file_size'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getFileId()   { return $this->file_id;    }
  public function getThumb()    { return $this->thumb;      }
  public function getFileName() { return $this->file_name;  }
  public function getMimeType() { return $this->mime_type;  }
  public function getFileSize() { return $this->file_size;  }
  // @codeCoverageIgnoreEnd
}
