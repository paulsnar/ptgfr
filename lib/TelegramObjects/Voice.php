<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class Voice extends AbstractObject
{
  private $file_id;
  private $duration;
  private $mime_type;
  private $file_size;

  protected $dc;

  public function __construct(
    string $file_id,
    int $duration,
    string $mime_type = null, /* @Optional */
    int $file_size = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->file_id = $file_id;
    $this->duration = $duration;
    $this->mime_type = $mime_type;
    $this->file_size = $file_size;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['mime_type', 'file_size'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    return (new Voice(
      $init['file_id'],
      $init['duration'],
      $init['mime_type'],
      $init['file_size'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getFileId()   { return $this->file_id;    }
  public function getDuration() { return $this->duration;   }
  public function getMimeType() { return $this->mime_type;  }
  public function getFileSize() { return $this->file_size;  }
  // @codeCoverageIgnoreEnd
}
