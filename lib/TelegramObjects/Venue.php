<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class Venue extends AbstractObject
{
  private $location;
  private $title;
  private $address;
  private $foursquare_id;

  protected $dc;

  public function __construct(
    Location $location,
    string $title,
    string $address,
    string $foursquare_id = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->location = $location;
    $this->title = $title;
    $this->address = $address;
    $this->foursquare_id = $foursquare_id;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    if (!array_key_exists('foursquare_id', $init)) {
      $init['foursquare_id'] = null;
    }

    $init['location'] = Location::fromJSON($init['location'], $dc);

    return (new Venue(
      $init['location'],
      $init['title'],
      $init['address'],
      $init['foursquare_id'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getLocation()     { return $this->location;       }
  public function getTitle()        { return $this->title;          }
  public function getAddress()      { return $this->address;        }
  public function getFoursquareId() { return $this->foursquare_id;  }
  // @codeCoverageIgnoreEnd
}
