<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class Audio extends AbstractObject
{
  private $file_id;
  private $duration;
  private $performer;
  private $title;
  private $mime_type;
  private $file_size;

  protected $dc;

  public function __construct(
    string $file_id,
    int $duration,
    string $performer = null, /* @Optional */
    string $title = null, /* @Optional */
    string $mime_type = null, /* @Optional */
    int $file_size = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->file_id = $file_id;
    $this->duration = $duration;
    $this->performer = $performer;
    $this->title = $title;
    $this->mime_type = $mime_type;
    $this->file_size = $file_size;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['performer', 'title', 'mime_type', 'file_size'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    return (new Audio(
      $init['file_id'],
      $init['duration'],
      $init['performer'],
      $init['title'],
      $init['mime_type'],
      $init['file_size'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getFileId()     { return $this->file_id;    }
  public function getDuration()   { return $this->duration;   }
  public function getPerformer()  { return $this->performer;  }
  public function getTitle()      { return $this->title;      }
  public function getMimeType()   { return $this->mime_type;  }
  public function getFileSize()   { return $this->file_size;  }
  // @codeCoverageIgnoreEnd
}
