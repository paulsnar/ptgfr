<?php

namespace Tgfr\TelegramObjects\Auxilinary;

use Tgfr\TelegramObjects\GameHighScore;

class GameHighScoreArray extends GenericArray
{
  public static function getContainedClass()
  {
    return GameHighScore::class;
  }

  protected function castValue($value, \Pimple\Container $dc = null)
  {
    return GameHighScore::fromJSON($value, $dc);
  }

  public static function fromJSON($items, \Pimple\Container $dc = null)
  {
    return new GameHighScoreArray($items, $dc);
  }
}
