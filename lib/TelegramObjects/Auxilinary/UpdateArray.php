<?php

namespace Tgfr\TelegramObjects\Auxilinary;

use Tgfr\TelegramObjects\Update;

class UpdateArray extends GenericArray
{
  public static function getContainedClass()
  {
    return Update::class;
  }

  protected function castValue($value, \Pimple\Container $dc = null)
  {
    return Update::fromJSON($value, $dc);
  }

  public static function fromJSON($items, \Pimple\Container $dc = null)
  {
    return new UpdateArray($items, $dc);
  }
}
