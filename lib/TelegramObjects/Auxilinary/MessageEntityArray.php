<?php

namespace Tgfr\TelegramObjects\Auxilinary;

use Tgfr\TelegramObjects\MessageEntity;

class MessageEntityArray extends GenericArray
{
  public static function getContainedClass()
  {
    return MessageEntity::class;
  }

  protected function castValue($value, \Pimple\Container $dc = null)
  {
    return MessageEntity::fromJSON($value, $dc);
  }

  public static function fromJSON($items, \Pimple\Container $dc = null)
  {
    return new MessageEntityArray($items, $dc);
  }
}
