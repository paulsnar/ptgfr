<?php

namespace Tgfr\TelegramObjects\Auxilinary;

abstract class AbstractObject
{
  private $init_json;

  public function setInitJSON($init_json)
  {
    $this->init_json = $init_json;
    return $this;
  }

  public function __toString()
  {
    return sprintf(
      '[%s] %s',
      get_class($this),
      json_encode($this->init_json, JSON_FORCE_OBJECT)
    );
  }
}
