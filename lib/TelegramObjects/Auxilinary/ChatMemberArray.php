<?php

namespace Tgfr\TelegramObjects\Auxilinary;

use Tgfr\TelegramObjects\ChatMember;

class ChatMemberArray extends GenericArray
{
  public static function getContainedClass()
  {
    return ChatMember::class;
  }

  protected function castValue($value, \Pimple\Container $dc = null)
  {
    return ChatMember::fromJSON($value, $dc);
  }

  public static function fromJSON($items, \Pimple\Container $dc = null)
  {
    return new ChatMemberArray($items, $dc);
  }
}
