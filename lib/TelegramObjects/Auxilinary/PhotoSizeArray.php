<?php

namespace Tgfr\TelegramObjects\Auxilinary;

use Tgfr\TelegramObjects\PhotoSize;

class PhotoSizeArray extends GenericArray
{
  public static function getContainedClass()
  {
    return PhotoSize::class;
  }

  protected function castValue($value, \Pimple\Container $dc = null)
  {
    return PhotoSize::fromJSON($value, $dc);
  }

  public static function fromJSON($items, \Pimple\Container $dc = null)
  {
    return new PhotoSizeArray($items, $dc);
  }
}
