<?php

namespace Tgfr\TelegramObjects\Auxilinary;

abstract class GenericArray implements \ArrayAccess, \IteratorAggregate
{
  protected $data;
  protected $dc;

  public function __construct(
    array $initArray = null,
    \Pimple\Container $dc = null)
  {
    $this->dc = $dc;

    $this->data = [ ];

    if ($initArray !== null && count($initArray) > 0) {
      foreach ($initArray as $key => $value) {
        $this->data[$key] = $this->castValue($value, $dc);
      }
    }
  }

  abstract public static function getContainedClass();

  abstract protected function castValue(
    $value,
    \Pimple\Container $dc = null
  );

  abstract public static function fromJSON(
    $items,
    \Pimple\Container $dc = null
  );

  public function offsetExists($offset)
  {
    return array_key_exists($offset, $this->data) || isset($data[$offset]);
  }

  public function offsetGet($offset)
  {
    return $this->offsetExists($offset) ? $this->data[$offset] : null;
  }

  public function offsetSet($offset, $value)
  {
    $type = static::getContainedClass();
    if (!($value instanceof $type)) {
      $value = $this->castValue($value, $this->dc);
    }

    if ($offset === null) {
      $this->data[] = $value;
    } else {
      $this->data[$offset] = $value;
    }
  }

  public function offsetUnset($offset)
  {
    unset($this->data[$offset]);
  }

  public function getIterator()
  {
    return new \ArrayIterator($this->data);
  }
}
