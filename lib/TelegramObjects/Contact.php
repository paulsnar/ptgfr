<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class Contact extends AbstractObject
{
  private $phone_number;
  private $first_name;
  private $last_name;
  private $user_id;

  protected $dc;

  public function __construct(
    string $phone_number,
    string $first_name,
    string $last_name = null, /* @Optional */
    int $user_id = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->phone_number = $phone_number;
    $this->first_name = $first_name;
    $this->last_name = $last_name;
    $this->user_id = $user_id;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['last_name', 'user_id'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    return (new Contact(
      $init['phone_number'],
      $init['first_name'],
      $init['last_name'],
      $init['user_id'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getPhoneNumber()  { return $this->phone_number; }
  public function getFirstName()    { return $this->first_name;   }
  public function getLastName()     { return $this->last_name;    }
  public function getUserId()       { return $this->user_id;      }
  // @codeCoverageIgnoreEnd
}
