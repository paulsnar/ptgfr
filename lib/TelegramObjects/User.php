<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class User extends AbstractObject
{
  private $id;
  private $first_name;
  private $last_name;
  private $username;

  protected $dc;

  public function __construct(
    int $id,
    string $first_name,
    string $last_name = null, /* @Optional */
    string $username = null, /* @Optional */
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->id = $id;
    $this->first_name = $first_name;
    $this->last_name = $last_name;
    $this->username = $username;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    $optionals = ['last_name', 'username'];
    foreach ($optionals as $optional) {
      if (!array_key_exists($optional, $init)) {
        $init[$optional] = null;
      }
    }

    return (new User(
      $init['id'],
      $init['first_name'],
      $init['last_name'],
      $init['username'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getId()         { return $this->id;         }
  public function getFirstName()  { return $this->first_name; }
  public function getLastName()   { return $this->last_name;  }
  public function getUsername()   { return $this->username;   }
  // @codeCoverageIgnoreEnd
}
