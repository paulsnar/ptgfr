<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;
use Tgfr\TelegramObjects\Auxilinary\PhotoSizeArray;

class UserProfilePhotos extends AbstractObject
{
  private $total_count;
  private $photos;

  protected $dc;

  public function __construct(
    int $total_count,
    array $photos,
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->total_count = $total_count;
    $this->photos = $photos;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    foreach ($init['photos'] as $key => $value) {
      $init['photos'][$key] = PhotoSizeArray::fromJSON($value);
    }

    return (new UserProfilePhotos(
      $init['total_count'],
      $init['photos'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getTotalCount() { return $this->total_count;  }
  public function getPhotos()     { return $this->photos;       }
  // @codeCoverageIgnoreEnd
}
