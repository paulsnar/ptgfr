<?php

namespace Tgfr\TelegramObjects;

use Tgfr\TelegramObjects\Auxilinary\AbstractObject;

class InlineQuery extends AbstractObject
{
  private $id;
  private $from;
  private $location;
  private $query;
  private $offset;

  protected $dc;

  public function __construct(
    string $id,
    User $from,
    Location $location = null, /* @Optional */
    string $query = null,
    string $offset = null,
    \Pimple\Container $dc = null /* @Optional */
  )
  {
    $this->id = $id;
    $this->from = $from;
    $this->location = $location;
    $this->query = $query;
    $this->offset = $offset;

    $this->dc = $dc;
  }

  public static function fromJSON($item, \Pimple\Container $dc = null)
  {
    $init = /* clone */ $item;

    if (!array_key_exists('location', $init)) {
      $init['location'] = null;
    }

    $init['from'] = User::fromJSON($init['from']);

    if ($init['location'] !== null) {
      $init['location'] = Location::fromJSON($init['location']);
    }

    return (new InlineQuery(
      $init['id'],
      $init['from'],
      $init['location'],
      $init['query'],
      $init['offset'],
      $dc
    ))->setInitJSON($item);
  }

  // @codeCoverageIgnoreStart
  public function getId()       { return $this->id;       }
  public function getFrom()     { return $this->from;     }
  public function getLocation() { return $this->location; }
  public function getQuery()    { return $this->query;    }
  public function getOffset()   { return $this->offset;   }
  // @codeCoverageIgnoreEnd

  public function answer(array $results, array $options = [ ])
  {
    return $this->dc['TelegramAPI']->invoke(
      'answerInlineQuery',
      [ 'inline_query_id' => $this->id,
        /* For some goddamn reason Telegram has to double decode things on
          their end, so we need to double encode them. :/ */
        'results' => json_encode($results) ] + $options);
  }
}
