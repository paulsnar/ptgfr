<?php

namespace Tgfr\Events\Routing;

class TextMessageEvent extends MessageEvent
{
  const NAME = 'tgfr.update.message.text';
}
