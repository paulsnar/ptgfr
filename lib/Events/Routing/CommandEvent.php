<?php

namespace Tgfr\Events\Routing;

use Tgfr\TelegramObjects\Message;

class CommandEvent extends TextMessageEvent
{
  const NAME = 'tgfr.update.message.command';

  protected $command;
  protected $args;
  protected $message;

  public function __construct(string $command, array $args, Message $message)
  {
    $this->command = $command;
    $this->args = $args;
    $this->message = $message;
  }

  public function getCommand()
  {
    return $this->command;
  }

  public function getArgs()
  {
    return $this->args;
  }

  public function getMessage()
  {
    return $this->message;
  }
}
