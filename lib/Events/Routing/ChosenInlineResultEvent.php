<?php

namespace Tgfr\Events\Routing;

use Symfony\Component\EventDispatcher\Event;
use Tgfr\TelegramObjects\ChosenInlineResult;

class ChosenInlineResultEvent extends Event
{
  const NAME = 'tgfr.update.chosen_inline_result';

  protected $chosen_inline_result;

  public function __construct(ChosenInlineResult $chosen_inline_result)
  {
    $this->chosen_inline_result = $chosen_inline_result;
  }

  public function getChosenInlineResult()
  {
    return $this->chosen_inline_result;
  }
}
