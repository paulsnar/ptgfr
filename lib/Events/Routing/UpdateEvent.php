<?php

namespace Tgfr\Events\Routing;

use Symfony\Component\EventDispatcher\Event;
use Tgfr\TelegramObjects\Update;

class UpdateEvent extends Event
{
  const NAME = 'tgfr.update';

  protected $update;

  public function __construct(Update $update)
  {
    $this->update = $update;
  }

  public function getUpdate()
  {
    return $this->update;
  }
}
