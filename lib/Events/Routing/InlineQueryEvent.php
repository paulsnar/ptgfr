<?php

namespace Tgfr\Events\Routing;

use Symfony\Component\EventDispatcher\Event;
use Tgfr\TelegramObjects\InlineQuery;

class InlineQueryEvent extends Event
{
  const NAME = 'tgfr.update.inline_query';

  protected $inline_query;

  public function __construct(InlineQuery $inline_query)
  {
    $this->inline_query = $inline_query;
  }

  public function getInlineQuery()
  {
    return $this->inline_query;
  }
}
