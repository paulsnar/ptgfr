<?php

namespace Tgfr\Events\Routing;

use Symfony\Component\EventDispatcher\Event;
use Tgfr\TelegramObjects\Message;

class MessageEvent extends Event
{
  const NAME = 'tgfr.update.message';

  protected $message;

  public function __construct(Message $message)
  {
    $this->message = $message;
  }

  public function getMessage()
  {
    return $this->message;
  }
}
