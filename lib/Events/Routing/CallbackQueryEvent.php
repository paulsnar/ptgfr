<?php

namespace Tgfr\Events\Routing;

use Symfony\Component\EventDispatcher\Event;
use Tgfr\TelegramObjects\CallbackQuery;

class CallbackQueryEvent extends Event
{
  const NAME = 'tgfr.update.callback_query';

  protected $callback_query;

  public function __construct(CallbackQuery $callback_query)
  {
    $this->callback_query = $callback_query;
  }

  public function getCallbackQuery()
  {
    return $this->callback_query;
  }
}
