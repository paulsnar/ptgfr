<?php

namespace Tgfr\Scaffolding;

use Tgfr\Bot;
use Tgfr\RequestHandler;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;
use Symfony\Component\HttpFoundation\Request;
use Pimple\Container;

class ControllerResolver implements ControllerResolverInterface, ArgumentResolverInterface
{
  protected $dc;

  public function __construct(Container $dc)
  {
    $this->dc = $dc;
  }

  public function getController(Request $request)
  {
    return new RequestHandler($this->dc);
  }

  public function getArguments(Request $request, $controller)
  {
    return [$request];
  }
}
