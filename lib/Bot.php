<?php

namespace Tgfr;

use Monolog\Logger;
use Monolog\Handler\NullHandler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;

use Tgfr\Scaffolding\ControllerResolver;
use Tgfr\TelegramObjects\Update;
use Tgfr\Events\Routing\UpdateEvent;
use Tgfr\TelegramAPI\CurlTelegramAPI;
use Tgfr\Routing\Router;

class Bot
{
  public static $framework_version = '0.1.0';

  protected $dc;
  protected $api_key;

  private $has_inline_method_called;
  private $inline_method_invocation;

  public function __construct(string $api_key)
  {
    $this->has_inline_method_called = false;

    $this->api_key = $api_key;
    $this->dc = new \Pimple\Container();

    $this->dc['Bot'] = function () {
      return $this;
    };

    $this->dc['EventDispatcher'] = function () {
      return new EventDispatcher();
    };
    $this->dc['ControllerResolver'] = function ($dc) {
      return new ControllerResolver($dc);
    };

    $this->dc['HttpKernel'] = function ($dc) {
      return new HttpKernel(
        $dc['EventDispatcher'],
        $dc['ControllerResolver'],
        new RequestStack(),
        $dc['ControllerResolver']
      );
    };

    $this->dc['Router'] = function () {
      return new Router($this->dc);
    };

    $this->dc['TelegramAPIInterface'] = function ($dc) {
      return new CurlTelegramAPI($dc);
    };
    $this->dc['TelegramAPI'] = function ($dc) {
      return new TelegramAPI($dc);
    };

    $this->dc['Log'] = function () {
      $l = new Logger('Tgfr');
      // Prevent messages being accidentally printed to stdout (?)
      $l->pushHandler(new NullHandler());
      return $l;
    };

    $this->initializeConfiguration();
  }

  public function getDependencyContainer()
  {
    return $this->dc;
  }

  public function getRouter()
  {
    return $this->dc['Router'];
  }

  public function getEventDispatcher()
  {
    return $this->dc['EventDispatcher'];
  }

  public function getApiKey()
  {
    return $this->api_key;
  }

  public function getFrameworkVersion()
  {
    return self::$framework_version;
  }

  protected function initializeConfiguration()
  {
    /**
     * Return method calls in the webhook response body.
     */
    $this->dc['config.auto_inline'] = true;

    /**
     * Require this key be present in the query parameters to ensure
     * request authenticity. Set to `null` to disable.
     */
    $this->dc['config.key'] = null;

    /**
     * Return an empty response before calling the command handler.
     *
     * Please note that enabling this option prevents inline responses from
     * working since the response will be sent *before* you have a chance to
     * generate one. But even then the tradeoff might be worth it.
     *
     * Uses `fastcgi_finish_request` internally, therefore has effect only on
     * the PHP FPM server. On other servers the response won't be sent before
     * the script finishes completely.
     */
    $this->dc['config.early_response'] = false;
  }

  public function handleRequest(Request $request)
  {
    $http_kernel = $this->dc['HttpKernel'];

    $response = $http_kernel->handle($request);
    $response->prepare($request);

    $response->send();

    $http_kernel->terminate($request, $response);
  }

  public function handleUpdate(Update $update)
  {
    $this->dc['Log']->debug(
      'Handling update', [ 'update' => $update ]);

    $this->dc['EventDispatcher']->dispatch(
      UpdateEvent::NAME, new UpdateEvent($update));

    $inline = $this->dc['TelegramAPI']->getCurrentInlineResponse();
    if ($inline !== null) {
      return new JsonResponse($inline);
    } else {
      return new Response(null, 204);
    }
  }
}
