<?php

namespace Tgfr\Routing;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Pimple\Container;

use Tgfr\TelegramObjects\Update;
use Tgfr\Events\Routing\UpdateEvent;
use Tgfr\Events\Routing\MessageEvent;
use Tgfr\Events\Routing\TextMessageEvent;
use Tgfr\Events\Routing\CommandEvent;
use Tgfr\Events\Routing\MessageEditedEvent;
use Tgfr\Events\Routing\ChannelPostEvent;
use Tgfr\Events\Routing\ChannelPostEditedEvent;
use Tgfr\Events\Routing\InlineQueryEvent;
use Tgfr\Events\Routing\ChosenInlineResultEvent;
use Tgfr\Events\Routing\CallbackQueryEvent;
use Tgfr\Routing\Command\CommandManager;
use Tgfr\Routing\Hotword\HotwordManager;
use Tgfr\Routing\Game\GameManager;

class Router implements EventSubscriberInterface
{
  public static function getSubscribedEvents()
  {
    return [
      UpdateEvent::NAME => 'determineUpdateType',
      KernelEvents::TERMINATE => 'handleKernelTerminated',
    ];
  }

  protected $dc;
  protected $update_handlers;
  protected $command_map;
  protected $queue;
  private $processing_queue;

  public function __construct(Container $dc)
  {
    $this->dc = $dc;
    $this->dc['EventDispatcher']->addSubscriber($this);

    $this->dc['Router.CommandManager'] = function ($dc) {
      return new CommandManager($dc);
    };
    $this->dc['Router.HotwordManager'] = function ($dc) {
      return new HotwordManager($dc);
    };
    $this->dc['Router.GameManager'] = function ($dc) {
      return new GameManager($dc);
    };

    $this->queue = [ ];
    $this->processing_queue = false;
  }

  public function getCommandManager()
  {
    return $this->dc['Router.CommandManager'];
  }

  public function getHotwordManager()
  {
    return $this->dc['Router.HotwordManager'];
  }

  public function getGameManager()
  {
    return $this->dc['Router.GameManager'];
  }

  protected function shouldQueueInstead()
  {
    if ($this->processing_queue) {
      return false;
    } else {
      return $this->dc['config.early_response'];
    }
  }

  public function determineUpdateType(UpdateEvent $update_event)
  {
    $update = $update_event->getUpdate();
    if ($this->shouldQueueInstead()) {
      $this->queue[] = $update_event->getUpdate();
      $update_event->stopPropagation();
      return;
    }

    $additional_events = [ ];

    if ($update->getMessage() !== null) {
      $message = $update->getMessage();

      $additional_events[] = [
        MessageEvent::NAME,
        new MessageEvent($message)
      ];

      if ($message->getText() !== null) {
        $message_text = $message->getText();
        $additional_events[] = [
          TextMessageEvent::NAME,
          new TextMessageEvent($message)
        ];

        if (mb_substr($message_text, 0, 1) === '/') {
          $args = explode(' ', $message_text);
          $command = array_shift($args);

          // Normalize command invocations from groups (/start@some_bot)
          if (mb_strpos($command, '@') !== false) {
            $command = mb_substr($command, 0, mb_strpos($command, '@'));
          }

          $additional_events[] = [
            CommandEvent::NAME,
            new CommandEvent($command, $args, $message)
          ];
        }
      }
    } else if ($update->getEditedMessage() !== null) {
      $additional_events[] = [
        MessageEditedEvent::NAME,
        new MessageEditedEvent($update->getEditedMessage()),
      ];
    } else if ($update->getChannelPost() !== null) {
      $additional_events[] = [
        ChannelPostEvent::NAME,
        new ChannelPostEvent($update->getChannelPost()),
      ];
    } else if ($update->getEditedChannelPost() !== null) {
      $additional_events[] = [
        ChannelPostEditedEvent::NAME,
        new ChannelPostEditedEvent($update->getEditedChannelPost()),
      ];
    } else if ($update->getInlineQuery() !== null) {
      $additional_events[] = [
        InlineQueryEvent::NAME,
        new InlineQueryEvent($update->getInlineQuery()),
      ];
    } else if ($update->getChosenInlineResult() !== null) {
      $additional_events[] = [
        ChosenInlineResultEvent::NAME,
        new ChosenInlineResultEvent($update->getChosenInlineResult()),
      ];
    } else if ($update->getCallbackQuery() !== null) {
      $additional_events[] = [
        CallbackQueryEvent::NAME,
        new CallbackQueryEvent($update->getCallbackQuery()),
      ];
    }

    $event_dispatcher = $this->dc['EventDispatcher'];

    foreach (array_reverse($additional_events) as $event_descriptor) {
      list($next_event_name, $next_event_instance) = $event_descriptor;
      $event_dispatcher->dispatch($next_event_name, $next_event_instance);
      if ($next_event_instance->isPropagationStopped()) {
        $update_event->stopPropagation();
        break;
      }
    }
  }

  public function handleKernelTerminated()
  {
    $this->processing_queue = true;

    // dispatch all the updates again
    $event_dispatcher = $this->dc['EventDispatcher'];
    foreach ($this->queue as $update) {
      $e = new UpdateEvent($update);
      $event_dispatcher->dispatch(UpdateEvent::NAME, $e);
    }

    $this->processing_queue = false;
  }
}
