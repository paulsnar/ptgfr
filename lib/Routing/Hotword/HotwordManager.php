<?php

namespace Tgfr\Routing\Hotword;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tgfr\Events\Routing\TextMessageEvent;
use Tgfr\Events\Routing\HotwordEvent;

class HotwordManager implements EventSubscriberInterface
{
  public static function getSubscribedEvents()
  {
    return [
      TextMessageEvent::NAME => 'processTextMessageEvent',
    ];
  }

  protected $dc;
  protected $hotwords;
  protected $regexes;

  public function __construct(\Pimple\Container $dc)
  {
    $this->dc = $dc;
    $this->dc['EventDispatcher']->addSubscriber($this);

    $this->hotwords = [ ];
    $this->regexes = [ ];
  }

  public function addHotword($word, $handler = null, $is_regex = false)
  {
    if ($word instanceof HotwordHandlerInterface &&
        $handler === null) {
      $handler = $word;
      $word = null;
    }

    if ($handler instanceof HotwordHandlerInterface) {
      if ($word === null) {
        $words = $handler::getHandledHotwords();
        $regexes = $handler::getHandledRegexes();
        $this->addHotword($words, $handler);
        $this->addHotword($regexes, $handler, true);
        return;
      } else if (is_array($word)) {
        foreach ($word as $addable_word) {
          $this->addHotword($addable_word, $handler, $is_regex);
        }
        return;
      }
    } else if (!is_callable($handler)) {
      throw new \InvalidArgumentException('Hotword handler should either be ' .
        'a callable or should implement ' . HotwordHandlerInterface::class);
    }

    if ($is_regex) {
      if (array_key_exists($word, $this->regexes)) {
        $this->regexes[$word][] = $handler;
      } else {
        $this->regexes[$word] = [ $handler ];
      }
    } else {
      if (array_key_exists($word, $this->hotwords)) {
        $this->hotwords[$word][] = $handler;
      } else {
        $this->hotwords[$word] = [ $handler ];
      }
    }
  }

  public function processTextMessageEvent(TextMessageEvent $message_event)
  {
    $message = $message_event->getMessage();

    $text = $message->getText();

    $stop = false;

    foreach ($this->regexes as $regex => $handlers) {
      if (preg_match($regex, $text)) {
        foreach ($handlers as $handler) {
          if ($handler instanceof HotwordHandlerInterface) {
            $stop = $handler->handle($regex, $message);
          } else {
            $stop = $handler($regex, $message, $this->dc['Bot']);
          }
          if ($stop) {
            $message_event->stopPropagation();
            return;
          }
        }
      }
    }

    $stop = false;
    foreach ($this->hotwords as $hotword => $handlers) {
      if (mb_strpos($text, $hotword) !== false) {
        foreach ($handlers as $handler) {
          if ($handler instanceof HotwordHandlerInterface) {
            $stop = $handler->handle($hotword, $message);
          } else {
            $stop = $handler($hotword, $message, $this->dc['Bot']);
          }
          if ($stop) {
            $message_event->stopPropagation();
            return;
          }
        }
      }
    }
  }
}
