<?php

namespace Tgfr\Routing\Hotword;

use Tgfr\TelegramObjects\Message;

interface HotwordHandlerInterface
{
  public static function getHandledHotwords();
  public static function getHandledRegexes();
  public function handle(string $trigger, Message $msg);
}
