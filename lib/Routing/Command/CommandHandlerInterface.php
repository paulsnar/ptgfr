<?php

namespace Tgfr\Routing\Command;

use Tgfr\TelegramObjects\Message;

interface CommandHandlerInterface
{
  public static function getHandledCommand();
  public function handleCommand(string $command, array $args, Message $msg);
}
