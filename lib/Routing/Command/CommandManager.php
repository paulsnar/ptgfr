<?php

namespace Tgfr\Routing\Command;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tgfr\Events\Routing\CommandEvent;

class CommandManager implements EventSubscriberInterface
{
  public static function getSubscribedEvents()
  {
    return [
      CommandEvent::NAME => 'processCommandEvent',
    ];
  }

  protected $dc;
  protected $commands;

  public function __construct(\Pimple\Container $dc)
  {
    $this->dc = $dc;
    $this->dc['EventDispatcher']->addSubscriber($this);

    $this->commands = [ ];
  }

  protected function validateCommand(string $command)
  {
    return preg_match('/\/[a-z0-9\_]+/i', $command) === 1;
  }

  public function handle($for_command, $handler = null)
  {
    if ($for_command instanceof CommandHandlerInterface &&
        $handler === null) {
      $handler = $for_command;
    }

    if ($handler instanceof CommandHandlerInterface) {
      $for_command = $handler::getHandledCommand();
    } else if (!is_callable($handler)) {
      throw new \InvalidArgumentException('Command handler should either be ' .
        'a callable or should implement ' . CommandHandlerInterface::class);
    }

    $this->validateCommand($for_command);
    $this->commands[$for_command] = $handler;
  }

  public function processCommandEvent(CommandEvent $command_event)
  {
    $command = $command_event->getCommand();
    $args = $command_event->getArgs();

    if (array_key_exists($command, $this->commands)) {
      $command_event->stopPropagation();
      $handler = $this->commands[$command];

      if ($handler instanceof CommandHandlerInterface) {
        $handler->handleCommand(
          $command_event->getCommand(),
          $command_event->getArgs(),
          $command_event->getMessage()
        );
      } else {
        $handler(
          $command_event->getCommand(),
          $command_event->getArgs(),
          $command_event->getMessage(),
          $this->dc['Bot']
        );
      }
    }
  }
}
