<?php

namespace Tgfr\Routing\Game;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tgfr\Events\Routing\CallbackQueryEvent;

class GameManager implements EventSubscriberInterface
{
  public static function getSubscribedEvents()
  {
    return [
      CallbackQueryEvent::NAME => 'processCallbackQueryEvent',
    ];
  }

  protected $dc;
  protected $games;

  public function __construct(\Pimple\Container $dc)
  {
    $this->dc = $dc;
    $this->dc['EventDispatcher']->addSubscriber($this);

    $this->games = [ ];
  }

  public function addGame($game, $url)
  {
    if (is_string($url)) {
      $handler = function ($query) use ($url) {
        return $url;
      };
    } else if (is_callable($url)) {
      $handler = $url;
    } else {
      throw new \InvalidArgumentException('Game handler should either be an ' .
        'URL as a string or a callable which returns one');
    }

    $this->games[$game] = $handler;
  }

  public function processCallbackQueryEvent(CallbackQueryEvent $event)
  {
    $callback_query = $event->getCallbackQuery();
    $game = $callback_query->getGameShortName();

    if ($game === null) { return; }

    if (array_key_exists($game, $this->games)) {
      $event->stopPropagation();
      $url = $this->games[$game]($callback_query);
      $callback_query->answer([ 'url' => $url ]);
    }
  }
}
