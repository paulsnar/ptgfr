<?php

namespace Tgfr;

use Tgfr\TelegramAPI\TelegramAPIInterface;
use Tgfr\TelegramObjects;

class TelegramAPI
{
  protected $dc;
  protected $inline;

  const GET_METHODS = [
    'getUpdates' => TelegramObjects\Auxilinary\UpdateArray::class,
    'getWebhookInfo' => TelegramObjects\WebhookInfo::class,
    'getMe' => TelegramObjects\User::class,
    'getUserProfilePhotos' => TelegramObjects\UserProfilePhotos::class,
    'getFile' => TelegramObjects\File::class,
    'getChat' => TelegramObjects\Chat::class,
    'getChatAdministrators' => TelegramObjects\Auxilinary\ChatMemberArray::class,
    'getChatMembersCount' => null,
    'getChatMember' => TelegramObjects\ChatMember::class,
    'getGameHighScores' => TelegramObjects\Auxilinary\GameHighScoreArray::class,
  ];

  const CALL_METHODS = [
    'setWebhook' => null,
    'deleteWebhook' => null,
    'sendMessage' => TelegramObjects\Message::class,
    'forwardMessage' => TelegramObjects\Message::class,
    'sendPhoto' => TelegramObjects\Message::class,
    'sendAudio' => TelegramObjects\Message::class,
    'sendDocument' => TelegramObjects\Message::class,
    'sendSticker' => TelegramObjects\Message::class,
    'sendVideo' => TelegramObjects\Message::class,
    'sendVoice' => TelegramObjects\Message::class,
    'sendLocation' => TelegramObjects\Message::class,
    'sendVenue' => TelegramObjects\Message::class,
    'sendContact' => TelegramObjects\Message::class,
    'sendChatAction' => null,
    'kickChatMember' => null,
    'leaveChat' => null,
    'unbanChatMember' => null,
    'answerCallbackQuery' => null,
    'editMessageText' => [TelegramObjects\Message::class, null],
    'editMessageCaption' => [TelegramObjects\Message::class, null],
    'editMessageReplyMarkup' => [TelegramObjects\Message::class, null],
    'answerInlineQuery' => null,
    'sendGame' => TelegramObjects\Message::class,
    'setGameScore' => [TelegramObjects\Message::class, null],
  ];

  public function __construct(\Pimple\Container $dc)
  {
    $this->dc = $dc;
    $this->inline = null;
  }

  public function getMethodType(string $method_name)
  {
    if (array_key_exists($method_name, self::GET_METHODS)) {
      return 'get';
    } else if (array_key_exists($method_name, self::CALL_METHODS)) {
      return 'call';
    } else {
      return null;
    }
  }

  protected function typecast($pristine_value, $type_spec)
  {
    if (is_array($type_spec)) {
      $casted_value = null;
      foreach ($type_spec as $type) {
        $casted_value = $this->typecast($pristine_value, $type);
        if ($casted_value !== null) {
          return $casted_value;
        }
      }

      // if (!$silent) {
      //   trigger_error(
      //     sprintf('Could not find a suitable cast for %s', $pristine_value),
      //     E_USER_WARNING
      //   );
      // }
      return null;
    } else if ($type_spec === null) {
      return $pristine_value; // primitive type
    } else {
      try {
        return $type_spec::fromJSON($pristine_value, $this->dc);
      } catch (\Exception $e) {
        // if (!$silent) {
        //   trigger_error(
        //     sprintf(
        //       'Encountered an error while trying to typecast a value: %s', $e),
        //     E_USER_WARNING
        //   );
        // }
        return null;
      }
    }
  }

  public function __call($name, $arguments)
  {
    return $this->invoke($name, $arguments[0]);
  }

  protected function canInline($method_name)
  {
    return
      !$this->dc['config.early_response'] &&
      $this->inline === null &&
      $this->getMethodType($method_name) === 'call'
    ;
  }

  protected function shouldInline($force_inline_mode)
  {
    if ($force_inline_mode === null) {
      return $this->dc['config.auto_inline'];
    } else {
      return $force_inline_mode;
    }
  }

  public function invoke(
    string $name,
    array $args = [ ],
    bool $inline_force = null
  ) {
    $this->dc['Log']->info(
      sprintf('Invoking %s', $name),
      [ 'args' => $args ]);

    $method_type = $this->getMethodType($name);
    if ($method_type === null) {
      throw new \BadMethodCallException(
        sprintf('Unrecognized method: "%s"', $name));
    }

    $can_inline = $this->canInline($name);
    $should_inline = $this->shouldInline($inline_force);

    // Refuse forced double inline calls
    if ($inline_force === true && !$can_inline) {
      throw new \LogicException('An inline method has already been called or '.
        'inlining is not available.');
    }

    if ($can_inline && $should_inline) {
      $this->dc['Log']->debug(sprintf('Inlining call to %s', $name));

      $this->inline = [ 'method' => $name ] + $args;
      return null;
    } else {
      $api = $this->dc['TelegramAPIInterface'];
      if (!($api instanceof TelegramAPIInterface)) {
        throw new \InvalidArgumentException(
          'Expected to have a TelegramAPIInterface in the dependency container');
      }

      if ($method_type === 'get') {
        $response = $api->getMethod($name, $args)['result'];
        $resolve_type = self::GET_METHODS[$name];
      } else if ($method_type === 'call') {
        $response = $api->callMethod($name, $args)['result'];
        $resolve_type = self::CALL_METHODS[$name];
      }
      return $this->typecast($response, $resolve_type);
    }
  }

  public function getCurrentInlineResponse()
  {
    return $this->inline;
  }
}
