<?php

namespace Tgfr;

use Tgfr\Bot;
use Tgfr\TelegramObjects\Update;
use Pimple\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RequestHandler
{
  protected $dc;

  public function __construct(Container $dc)
  {
    $this->dc = $dc;
  }

  public function __invoke(Request $request)
  {
    if ($this->dc['config.key'] !== null) {
      if (!$request->query->has('key')) {
        return new Response(null, Response::HTTP_UNAUTHORIZED);
      } else if ($request->query->get('key') !== $this->dc['config.key']) {
        return new Response(null, Response::HTTP_FORBIDDEN);
      }
    }
    $format = $request->getContentType();

    if ($format === 'json') {
      $body = $request->getContent();
      $data = json_decode($body, true);
      if ($data === null) {
        $e = json_last_error();
        if ($e !== JSON_ERROR_NONE) {
          return new Response(
            'Error: JSON could not be processed.',
            Response::HTTP_BAD_REQUEST,
            [ 'Content-Type' => 'text/plain' ]
          );
        }
      }

      $update = Update::fromJSON($data, $this->dc);

      return $this->dc['Bot']->handleUpdate($update);
    } else if ($format === null) {
      return new Response(
        'Error: Request did not have a body.',
        Response::HTTP_BAD_REQUEST,
        [ 'Content-Type' => 'text/plain' ]
      );
    } else {
      return new Response(
        sprintf('Error: Unsupported request format: "%s"',
          $request->getMimeType($format)),
        Response::HTTP_UNSUPPORTED_MEDIA_TYPE,
        [ 'Content-Type' => 'text/plain' ]
      );
    }
  }
}
