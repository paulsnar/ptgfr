# ptgfr - tgfr for PHP

Webhook-based Telegram bot framework.

## Example usage

The instance you've made by cloning is already prepared to use! Here is a short
summary to get you up and running quickly:

1. Ensure you have [Composer][]. If you don't,
  [follow the download guide][composer-download].
2. Run `php composer.phar install` (or `composer install`, if it's installed
  globally).
3. Set up your stuff in the files in the `src/` directory. A couple of examples
  is already there.
4. Set the `public` directory as your world-exposed HTML root *or* copy the
  `index.php` within that directory to your existing HTML root and change the
  require path to match where you've put the framework directory (e.g. where
  you're in right now). Change the `TELEGRAM_APIKEY` constant to match what
  you've gotten from Botfather.
5. And after that you can run `php bin/console telegram:set-webhook
  <your-webhook-domain>/index.php`. That should set Telegram up to send updates
  over to your server as they come in.
6. There is no sixth step. That's pretty much it!

[Composer]: https://getcomposer,org
[composer-download]: https://getcomposer.org/download/

## Polling

In case it's not possible for you to set up a webhook, you can also use the
built-in console command `tgfr:run-poller` to get updates in a long-polling
fashion. Please note that **this way of using this framework is not supported**
and might not work properly.

But if you still need it, follow the steps in the *Example usage* section up
until the last one, then run `php bin/console tgfr:run-poller`. It will launch
a polling loop which will continue as long as you keep the console open.
