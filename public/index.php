<?php

require __DIR__ . '/../vendor/autoload.php';

define('TELEGRAM_APIKEY', getenv('TELEGRAM_APIKEY'));

$bot = new Tgfr\Bot(TELEGRAM_APIKEY);

Bot\Setup::install($bot);

$bot->run(Symfony\Component\HttpFoundation\Request::createFromGlobals());
