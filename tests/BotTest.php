<?php

use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

use Tgfr\Bot;
use Tgfr\TelegramAPI\TelegramAPIInterface;

class BotTest extends TestCase
{
  protected $api_key;
  protected $bot;

  public function setUp()
  {
    $this->api_key = 'APIKEY_' . rand();
    $this->bot = new Bot($this->api_key);
  }

  public function tearDown()
  {
    \Mockery::close();
  }

  public function updateProvider()
  {
    return [
      [
        json_encode([
          'update_id' => 1000,
          'message' => [
            'message_id' => 1200,
            'from' => [
              'id' => 1400,
              'first_name' => 'Test User',
            ],
            'date' => time(),
            'chat' => [
              'id' => 1400,
              'type' => 'private',
              'first_name' => 'Test User',
            ],
            'text' => '/test',
          ],
        ], JSON_FORCE_OBJECT),
      ],
    ];
  }

  /**
   * @dataProvider updateProvider
   */
  public function testInlineMethodCalling($str_update)
  {
    $update = json_decode($str_update, true);
    $message = $update['message'];

    $request = new Request(
      /* $query = */ [ ],
      /* $request = */ [ ],
      /* $attributes = */ [ ],
      /* $cookies = */ [ ],
      /* $files = */ [ ],
      /* $server =  */ [ 'HTTP_CONTENT_TYPE' => 'application/json; charset=utf-8' ],
      $str_update
    );

    $response_array = [
      'chat_id' => $message['chat']['id'],
      'text' => 'Test text',
    ];

    $dc = $this->bot->getDependencyContainer();

    $this->bot->getRouter()->getCommandManager()
      ->handle('/test', function () use ($dc, $response_array) {
        $dc['TelegramAPI']->invoke(
          'sendMessage',
          $response_array,
          true
        );
        return null;
      });

    $dc['EventDispatcher']->addListener(
      KernelEvents::RESPONSE,
      function (FilterResponseEvent $e)
      use ($response_array) {
        $response = $e->getResponse();

        $response_full_array = /* clone */ $response_array;
        $response_full_array['method'] = 'sendMessage';

        $response_body = $response->getContent();
        $response_parsed_body = json_decode($response_body, true);

        $this->assertTrue(
          mb_strlen($response_body) > 0,
          'Response body was empty - (probably) an error occured.'
        );

        $this->assertNotContains(
          'Error: ',
          $response_body,
          'Response contained a handling error.'
        );

        $this->assertContains(
          'application/json',
          $response->headers->get('Content-Type'),
          'Response was not sent with a JSON Content-Type'
        );

        $this->assertEquals(
          $response_full_array,
          json_decode($response->getContent(), true),
          'Response did not contain a properly formatted inline method call.'
        );

        // prevent any output from going to the console and PHPUnit bitching
        // about it
        $r = new Response(null, Response::HTTP_NO_CONTENT);
        $e->setResponse($r);
      }
    );

    $this->bot->handleRequest($request);
  }

  /**
   * @dataProvider updateProvider
   */
  public function testNonInlineMethodCalling($str_update)
  {
    $update = json_decode($str_update, true);
    $message = $update['message'];

    $request = new Request([ ], [ ], [ ], [ ], [ ],
      [ 'HTTP_CONTENT_TYPE' => 'application/json' ],
      $str_update);

    $dc = $this->bot->getDependencyContainer();
    $dc['TelegramAPI'] = \Mockery::mock('Tgfr\TelegramAPI')
      ->shouldReceive('getCurrentInlineResponse')
      ->andReturn(null)
      ->mock();

    $dc['EventDispatcher']->addListener(KernelEvents::RESPONSE,
      function (FilterResponseEvent $e) {
        $response = $e->getResponse();

        $this->assertEquals(Response::HTTP_NO_CONTENT,
          $response->getStatusCode());
      });

    $this->bot->handleRequest($request);
  }

  public function testDependencyContainerAccessors()
  {
    // $this->assertEquals($this->dc, $this->bot->getDependencyContainer());
    $dc = $this->bot->getDependencyContainer();
    $this->assertEquals($dc['Router'], $this->bot->getRouter());
    $this->assertEquals($dc['EventDispatcher'],
      $this->bot->getEventDispatcher());
  }

  public function testPropertyAccessors()
  {
    $this->assertEquals($this->api_key, $this->bot->getApiKey());
    $this->assertEquals(Bot::$framework_version,
      $this->bot->getFrameworkVersion());
  }

  public function testDependencyContainerTelegramAPIInstance()
  {
    $dc = $this->bot->getDependencyContainer();
    $this->assertInstanceOf(TelegramAPIInterface::class,
      $dc['TelegramAPIInterface']);
  }
}
