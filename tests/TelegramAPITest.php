<?php

use PHPUnit\Framework\TestCase;

use Pimple\Container;

use Tgfr\TelegramAPI;
use Tgfr\TelegramAPI\TelegramAPIInterface;
use Tgfr\TelegramObjects\User;

class TelegramAPITest extends TestCase
{
  protected $dc;
  protected $api;
  protected $api_interface;

  public function setUp()
  {
    $this->dc = new Container();
    $this->dc['config.auto_inline'] = false;
    $this->dc['config.early_response'] = false;
    $this->dc['TelegramAPIInterface'] = function () {
      return $this->api_interface;
    };
    $this->dc['Log'] = function () {
      $l = new \Monolog\Logger('(test) Tgfr');
      $l->pushHandler(new \Monolog\Handler\NullHandler());
      return $l;
    };

    $this->api = new TelegramAPI($this->dc);
  }

  public function tearDown()
  {
    \Mockery::close();
  }

  protected function generateSelf()
  {
    return [
      'id' => 1000,
      'first_name' => 'Test Bot',
      'username' => 'test_bot',
    ];
  }

  protected function generateUser()
  {
    static $id = 1000;
    $user = [
      'id' => $id++,
      'first_name' => "User",
      'last_name' => "$id",
      'username' => "user_$id",
    ];
    return $user;
  }

  protected function generateChat($type = 'private')
  {
    static $id = 2000;
    if ($type === 'private') {
      $user = $this->generateUser();
      $chat = [
        'type' => $type,
      ];
      $chat = $chat + $user;
      return $chat;
    } else if ($type === 'group') {
      return [
        'type' => $type,
        'id' => $id++,
        'title' => 'Group Chat',
        'all_members_are_administrators' => true,
      ];
    } else if ($type === 'channel') {
      return [
        'type' => $type,
        'id' => $id++,
        'title' => 'Channel Chat',
        'username' => 'testchan',
      ];
    } else {
      throw new \InvalidArgumentException(
        sprintf('Unrecognized chat type: "%s"', $type));
    }
  }

  protected function generateOutgoingMessage($chat, $text, $other_entity = null)
  {
    static $id = 15000;
    $message = [
      'message_id' => $id++,
      'from' => $this->generateSelf(),
      'chat' => $chat,
      'text' => $text,
      'date' => time(),
    ];

    if ($other_entity !== null) {
      unset($message['text']);
      $message[$other_entity->type] = $other_entity->data;
    }

    return $message;
  }

  protected function generateForwardedMessage($to_chat, $original_message)
  {
    $new_message = $this->generateOutgoingMessage(
        $to_chat, $original_message['text']) + [
      'forward_from' => $original_message['from'],
      'forward_date' => $original_message['date'],
    ];
    return $new_message;
  }

  protected function generateFile()
  {
    static $id = 35000;
    return [
      'file_id' => 'file_' . $id,
      'file_size' => rand(1000, 1000000),
    ];
  }

  protected function generatePhotoSize()
  {
    $file = $this->generateFile();
    return [
      'file_id' => $file['file_id'],
      'file_size' => $file['file_size'],
      'width' => rand(100, 1920),
      'height' => rand(100, 1080),
    ];
  }

  protected function generateLocation()
  {
    return [
      'longitude' => rand(0, 180) + (rand() / getrandmax()),
      'latitude' => rand(0, 90) + (rand() / getrandmax()),
    ];
  }

  public function getMethodArgProvider()
  {
    $u = $this->generateUser();
    $file = $this->generateFile();
    $c = $this->generateChat('channel');

    return [
      [
        'getUpdates', [ 'offset' => 1000, 'limit' => 5 ],
        [ 'ok' => true,
          'result' => [ [ 'update_id' => 1000 ], [ 'update_id' => 1001 ] ] ],
      ], [
        'getWebhookInfo', [ ],
        [ 'ok' => true,
          'result' =>
          [ 'url' => '',
            'has_custom_certificate' => false,
            'pending_update_count' => 0, ], ],
      ], [
        'getMe', [ ],
        [ 'ok' => true, 'result' => $this->generateSelf(), ],
      ], [
        'getUserProfilePhotos', [ 'user_id' => $u['id'] ],
        [
          'ok' => true,
          'result' => [
            'total_count' => 3,
            'photos' => [
              [ $this->generatePhotoSize(),
                $this->generatePhotoSize(),
                $this->generatePhotoSize() ],
              [ $this->generatePhotoSize(),
                $this->generatePhotoSize(),
                $this->generatePhotoSize() ],
              [ $this->generatePhotoSize(),
                $this->generatePhotoSize(),
                $this->generatePhotoSize() ],
            ],
          ],
        ],
      ], [
        'getFile', [ 'file_id' => $file['file_id'] ],
        [ 'ok' => true, 'result' => $file ],
      ], [
        'getChat', [ 'chat_id' => '@testchan' ],
        [ 'ok' => true, 'result' => $c, ],
      ], [
        'getChatAdministrators', [ 'chat_id' => '@testchan' ],
        [ 'ok' => true,
          'result' => [ [ 'user' => $u, 'status' => 'creator', ] ] ]
      ], [
        'getChatMembersCount', [ 'chat_id' => '@testchan' ],
        [ 'ok' => true, 'result' => 5 ],
      ], [
        'getChatMember', [ 'chat_id' => '@testchan', 'user_id' => $u['id'] ],
        [ 'ok' => true, 'result' => [ 'user' => $u, 'status' => 'creator', ] ],
      ], [
        'getGameHighScores',
        [ 'user_id' => $u['id'],
          'inline_message_id' => 'inline_' . rand() ],
        [ 'ok' => true,
          'result' => [
            [ 'position' => 4,
              'user' => $this->generateUser(),
              'score' => 150 ],
            [ 'position' => 5,
              'user' => $u,
              'score' => 130 ],
            [ 'position' => 6,
              'user' => $this->generateUser(),
              'score' => 110 ] ] ],
      ],
    ];
  }

  public function callMethodArgProvider()
  {
    $c = $this->generateChat();
    $chat_2 = $this->generateChat();
    $g = $this->generateChat('group');

    $m = $this->generateOutgoingMessage($c, 'Test!');
    $msg_2 = $this->generateForwardedMessage($chat_2, $m);

    $photo = [ $this->generatePhotoSize(), $this->generatePhotoSize() ];
    $audio = $this->generateFile() + [ 'duration' => rand(60, 300) ];
    $document = $this->generateFile() +
      [ 'thumb' => $this->generatePhotoSize() ];
    $sticker = $this->generatePhotoSize() +
      [ 'thumb' => $this->generatePhotoSize() ];
    $video = $this->generatePhotoSize() +
      [ 'duration' => rand(5, 100), 'thumb' => $this->generatePhotoSize() ];
    $voice = $this->generateFile() + [ 'duration' => rand(10, 30) ];
    $contact =[
      'phone_number' => '+1 907 456 7890',
      'first_name' => 'Mr.Bear',
    ];
    $location = $this->generateLocation();
    $venue = [
      'location' => $this->generateLocation(),
      'title' => 'Baker Street',
      'address' => 'Nowhere',
    ];
    $game = [
      'title' => 'Test game!',
      'description' => 'Test game description.',
      'photo' => [ $this->generatePhotoSize(), $this->generatePhotoSize() ],
    ];

    $photo_msg = $this->generateOutgoingMessage(
      $c, null, (object) [ 'type' => 'photo', 'data' => $photo ]);
    $photo_msg['caption'] = 'Test caption!';

    $audio_msg = $this->generateOutgoingMessage(
      $c, null, (object) [ 'type' => 'audio', 'data' => $audio ]);
    $document_msg = $this->generateOutgoingMessage(
      $c, null, (object) [ 'type' => 'document', 'data' => $document ]);
    $sticker_msg = $this->generateOutgoingMessage(
      $c, null, (object) [ 'type' => 'sticker', 'data' => $sticker ]);
    $video_msg = $this->generateOutgoingMessage(
      $c, null, (object) [ 'type' => 'video', 'data' => $video ]);
    $voice_msg = $this->generateOutgoingMessage(
      $c, null, (object) [ 'type' => 'voice', 'data' => $voice ]);
    $contact_msg = $this->generateOutgoingMessage(
      $c, null, (object) [ 'type' => 'contact', 'data' => $contact ]);
    $location_msg = $this->generateOutgoingMessage(
      $c, null, (object) [ 'type' => 'location', 'data' => $location ]);
    $venue_msg = $this->generateOutgoingMessage(
      $c, null, (object) [ 'type' => 'venue', 'data' => $venue ]);
    $game_msg = $this->generateOutgoingMessage(
      $c, null, (object) [ 'type' => 'game', 'data' => $game ]);

    return [
      [
        'setWebhook', [ 'url' => 'https://example.com' ],
        [ 'ok' => true, 'result' => true ],
      ], [
        'deleteWebhook', [ ],
        [ 'ok' => true, 'result' => true ],
      ], [
        'sendMessage', [ 'chat_id' => $c['id'], 'text' => 'Test!' ],
        [ 'ok' => true, 'result' => $m ],
      ], [
        'forwardMessage',
        [ 'chat_id' => $chat_2['id'],
          'from_chat_id' => $c['id'],
          'message_id' => $m['message_id'] ],
        [ 'ok' => true, 'result' => $msg_2 ],
      ], [
        'sendPhoto', [ 'chat_id' => $c['id'], 'photo' => 'file_1000' ],
        [ 'ok' => true, 'result' => $photo_msg ],
      ], [
        'sendAudio',
        [ 'chat_id' => $c['id'],
          'audio' => $audio['file_id'] ],
        [ 'ok' => true, 'result' => $audio_msg ],
      ], [
        'sendDocument',
        [ 'chat_id' => $c['id'],
          'document' => $document['file_id'] ],
        [ 'ok' => true, 'result' => $document_msg ],
      ], [
        'sendSticker',
        [ 'chat_id' => $c['id'],
          'sticker' => $sticker['file_id'] ],
        [ 'ok' => true, 'result' => $sticker_msg ],
      ], [
        'sendVideo',
        [ 'chat_id' => $c['id'],
          'video' => $video['file_id'] ],
        [ 'ok' => true, 'result' => $video_msg ],
      ], [
        'sendVoice',
        [ 'chat_id' => $c['id'],
          'voice' => $voice['file_id'] ],
        [ 'ok' => true, 'result' => $voice_msg ],
      ], [
        'sendLocation',
        [ 'chat_id' => $c['id'],
          'longitude' => $location['longitude'],
          'latitude' => $location['latitude'] ],
        [ 'ok' => true, 'result' => $location_msg ],
      ], [
        'sendVenue',
        [ 'chat_id' => $c['id'],
          'longitude' => $venue['location']['longitude'],
          'latitude' => $venue['location']['latitude'],
          'title' => $venue['title'],
          'address' => $venue['address'] ],
        [ 'ok' => true, 'result' => $venue_msg ],
      ], [
        'sendContact',
        [ 'chat_id' => $c['id'],
          'phone_number' => $contact['phone_number'],
          'first_name' => $contact['first_name'] ],
        [ 'ok' => true, 'result' => $contact_msg ],
      ], [
        'sendChatAction', [ 'chat_id' => $c['id'], 'action' => 'typing' ],
        [ 'ok' => true, 'result' => true ],
      ], [
        'kickChatMember', [ 'chat_id' => $g['id'], 'user_id' => $c['id'] ],
        [ 'ok' => true, 'result' => true ],
      ], [
        'leaveChat', [ 'chat_id' => $g['id'] ],
        [ 'ok' => true, 'result' => true ],
      ], [
        'unbanChatMember', [ 'chat_id' => $g['id'], 'user_id' => $c['id'] ],
        [ 'ok' => true, 'result' => true ],
      ], [
        'answerCallbackQuery', [ 'callback_query_id' => 'cbq_' . rand() ],
        [ 'ok' => true, 'result' => true ],
      ], [
        'editMessageText',
        [ 'chat_id' => $c['id'],
          'message_id' => $m['message_id'],
          'text' => 'Edited text' ],
        [ 'ok' => true, 'result' => [ 'text' => 'Edited text' ] + $m ],
      ], [
        'editMessageCaption',
        [ 'chat_id' => $c['id'],
          'message_id' => $photo_msg['message_id'],
          'caption' => 'Edited caption' ],
        [ 'ok' => true,
          'result' => [ 'caption' => 'Edited caption' ] + $photo_msg ],
      // ], [
      //   'editMessageReplyMarkup', [], [] // TODO eventually?
      ], [
        'answerInlineQuery',
        [ 'inline_query_id' => 'inline_' . rand(),
          'results' => [ ] ],
        [ 'ok' => true, 'result' => true ],
      ], [
        'sendGame',
        [ 'chat_id' => $c['id'],
          'game_short_name' => 'test_game' ],
        [ 'ok' => true, 'result' => $game_msg ],
      ], [
        'setGameScore',
        [ 'user_id' => $c['id'],
          'score' => 131,
          'inline_message_id' => 'inline_' . rand() ],
        [ 'ok' => true, 'result' => true ],
      ],
    ];
  }

  protected function assertCastValue($cast_value, $actual_response, $response)
  {
    if ($cast_value === null) {
      $this->assertEquals(
        $response['result'], $actual_response,
        sprintf('Typecaster did not return primitive (instead got: %s)',
          var_export($actual_response, true))
      );
    } else if (is_array($cast_value)) {
      $found = false;
      foreach ($cast_value as $cast_value_option) {
        if ($cast_value_option === null) {
          $this->assertEquals($response['result'], $actual_response);
          return;
        } else {
          if ($actual_response instanceof $cast_value_option) {
            $this->assertInstanceOf($cast_value_option, $actual_response);
            return;
          }
        }
      }
      $this->fail('Typecaster did not return an object that would suit ' .
        'any of the available types');
    } else {
      $this->assertInstanceOf($cast_value, $actual_response);
    }
  }

  /**
   * @dataProvider getMethodArgProvider
   */
  public function testGetMethods($name, $args, $response)
  {
    $this->api_interface =
      \Mockery::mock('Tgfr\TelegramAPI\TelegramAPIInterface')
        ->shouldReceive('getMethod')->once()
        ->withArgs([$name, $args])
        ->andReturn($response)
        ->mock();

    $actual_response = $this->api->$name($args);

    $cast_value = TelegramAPI::GET_METHODS[$name];
    $this->assertCastValue($cast_value, $actual_response, $response);
  }

  /**
   * @dataProvider callMethodArgProvider
   */
  public function testCallMethods($name, $args, $response)
  {
    $this->api_interface =
      \Mockery::mock('Tgfr\\TelegramAPI\\TelegramAPIInterface')
        ->shouldReceive('callMethod')->once()
        ->withArgs([$name, $args])
        ->andReturn($response)
        ->mock();

    $actual_response = $this->api->invoke($name, $args);

    $cast_target = TelegramAPI::CALL_METHODS[$name];
    $this->assertCastValue($cast_target, $actual_response, $response);
  }

  public function testInvalidMethodCall()
  {
    $this->expectException(\BadMethodCallException::class);
    $this->api->invoke('unknown_method_' . rand(), [ ]);
  }

  public function testDoubleForcedInlineMethodCall()
  {
    $this->expectException(\LogicException::class);

    $this->api->invoke('sendMessage', [ ], true);
    $this->api->invoke('sendMessage', [ ], true);
  }

  public function testDoubleInlineMethodCall()
  {
    $this->api_interface =
      \Mockery::mock('Tgfr\\TelegramAPI\\TelegramAPIInterface')
        ->shouldReceive('callMethod')->once()
        ->with('sendMessage', [ ])
        ->andReturn([
          'ok' => true,
          'result' => [ 'message_id' => rand(), 'date' => time(),
            'chat' => [ 'id' => rand(), 'type' => 'group', 'title' => 'G' ] ]
        ])
        ->mock();

    $this->dc['config.auto_inline'] = true;
    $this->api->invoke('sendMessage', [ ]);
    $this->api->invoke('sendMessage', [ ]);

    $this->assertTrue(true); // pass!
  }

  public function testMissingTelegramAPIInterface()
  {
    $this->expectException(\InvalidArgumentException::class);

    $this->api->invoke('getMe', [ ]);
  }

  public function testTypecastImpossibleValue()
  {
    $this->api_interface =
      \Mockery::mock('Tgfr\\TelegramAPI\\TelegramAPIInterface')
        ->shouldReceive('callMethod')->once()
        ->with('setGameScore', [ ])
        ->andReturn([
          'ok' => true,
          'result' => null,
        ])
        ->mock();

    $this->assertNull($this->api->invoke('setGameScore', [ ]));
  }
}
