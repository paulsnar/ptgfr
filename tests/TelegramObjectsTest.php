<?php

use PHPUnit\Framework\TestCase;

use Pimple\Container;
use Tgfr\TelegramObjects;

class TelegramObjectsTest extends TestCase
{
  protected $dc;

  public function setUp()
  {
    $this->dc = new Container();
  }

  public function tearDown()
  {
    \Mockery::close();
  }

  public function convenienceMethodDataProvider()
  {
    $user = [ 'id' => rand(), 'first_name' => 'User' ];
    $user_tc = TelegramObjects\User::fromJSON($user);

    $bot = [ 'id' => rand(), 'first_name' => 'Bot' ];

    $chat = [ 'id' => rand(), 'type' => 'group', 'title' => 'Group' ];

    $callback_query = [
      'id' => 'callback_' . rand(),
      'from' => $user,
      'chat_instance' => implode('', [ rand(), rand(), rand() ]),
      'data' => json_encode([ 'do' => 'something' ], JSON_FORCE_OBJECT),
    ];

    $inline_query = [
      'id' => 'inline_' . rand(),
      'from' => $user,
      'query' => 'Test query',
      'offset' => ''
    ];

    $inline_article = [
      'type' => 'article', 'id' => '1', 'title' => 'Test Article',
      'input_message_content' => [ 'message_text' => 'Test Article?' ]
    ];

    $message = [
      'message_id' => rand(),
      'from' => $user,
      'date' => time(),
      'chat' => [ 'type' => 'private' ] + $user,
      'text' => 'Test text',
    ];

    return [
      [
        TelegramObjects\CallbackQuery::class,
        $callback_query,
        'answer',
        ['Response text'],
        'answerCallbackQuery',
        [ 'callback_query_id' => $callback_query['id'],
          'text' => 'Response text' ],
        true,
      ], [
        TelegramObjects\Chat::class,
        $chat,
        'leave',
        [ ],
        'leaveChat',
        [ 'chat_id' => $chat['id'] ],
        true,
      ], [
        TelegramObjects\Game::class,
        [ 'title' => 'Test game', 'description' => '',
          'photo' =>
          [ [ 'file_id' => (string) rand(),
              'width' => 10, 'height' => 10 ] ] ],
        'setScore',
        [ $user_tc, 150 ],
        'setGameScore',
        [ 'user_id' => $user_tc->getId(), 'score' => 150 ],
        true,
      ], [
        TelegramObjects\InlineQuery::class,
        $inline_query,
        'answer',
        [ [ $inline_article ] ],
        'answerInlineQuery',
        [ 'inline_query_id' => $inline_query['id'],
          'results' => json_encode([ $inline_article ]) ],
        true,
      ], [
        TelegramObjects\Message::class,
        $message,
        'respond',
        [ 'Test response' ],
        'sendMessage',
        [ 'chat_id' => $message['chat']['id'],
          'text' => 'Test response' ],
        TelegramObjects\Message::fromJSON([
          'message_id' => rand(),
          'from' => $bot,
          'date' => time(),
          'chat' => [ 'type' => 'private' ] + $user,
          'text' => 'Test response',
        ]),
      ], [
        TelegramObjects\Message::class,
        $message,
        'reply',
        [ 'Test reply' ],
        'sendMessage',
        [ 'chat_id' => $message['chat']['id'],
          'text' => 'Test reply',
          'reply_to_message_id' => $message['message_id'] ],
        TelegramObjects\Message::fromJSON([
          'message_id' => rand(),
          'from' => $bot,
          'date' => time(),
          'chat' => [ 'type' => 'private' ] + $user,
          'text' => 'Test reply',
          'reply_to_message' => $message,
        ]),
      ], [
        TelegramObjects\Message::class,
        $message,
        'forwardTo',
        [ $chat['id'] ],
        'forwardMessage',
        [ 'chat_id' => $chat['id'],
          'from_chat_id' => $message['chat']['id'],
          'message_id' => $message['message_id'] ],
        TelegramObjects\Message::fromJSON([
          'message_id' => rand(),
          'from' => $bot,
          'date' => time(),
          'chat' => $chat,
          'text' => $message['text'],
          'forward_date' => $message['date'],
          'forward_from' => $message['from'],
        ]),
      ], [
        TelegramObjects\Message::class,
        $message,
        'editText',
        [ 'New text!' ],
        'editMessageText',
        [ 'chat_id' => $message['chat']['id'],
          'message_id' => $message['message_id'],
          'text' => 'New text!' ],
        TelegramObjects\Message::fromJSON([
          'edit_date' => time(),
          'text' => 'New text!',
        ] + $message),
      ], [
        TelegramObjects\Message::class,
        $message,
        'editCaption',
        [ 'New caption!' ],
        'editMessageCaption',
        [ 'chat_id' => $message['chat']['id'],
          'message_id' => $message['message_id'],
          'caption' => 'New caption!' ],
        TelegramObjects\Message::fromJSON([
          'edit_date' => time(),
          'caption' => 'New caption!',
        ] + $message),
      ], [
        TelegramObjects\Message::class,
        $message,
        'editReplyMarkup',
        [ [ [ 'force_reply' => true ] ] ],
        'editMessageReplyMarkup',
        [ 'chat_id' => $message['chat']['id'],
          'message_id' => $message['message_id'],
          'reply_markup' => [ [ 'force_reply' => true ] ] ],
        TelegramObjects\Message::fromJSON($message),
      ],
    ];
  }

  /**
   * @dataProvider convenienceMethodDataProvider
   */
  public function testConvenienceMethods(
    $class,
    $json,
    $call_method,
    $call_args,
    $expected_method_call,
    $expected_method_args,
    $expected_response
  )
  {
    $this->dc['TelegramAPI'] =
    function ()
    use ($expected_method_call, $expected_method_args, $expected_response)
    {
      return \Mockery::mock('TelegramAPI',
        function ($mock)
        use ($expected_method_call, $expected_method_args, $expected_response)
        {
          $mock->shouldReceive('invoke')
          ->with($expected_method_call, $expected_method_args)
          ->andReturn($expected_response);
        }
      );
    };
    $obj = $class::fromJSON($json, $this->dc);

    $response = call_user_func_array([$obj, $call_method], $call_args);
    $this->assertEquals($expected_response, $response);
  }

  public function makeFloat()
  {
    return rand() + (rand() / getrandmax());
  }

  public function objectDataProvider()
  {
    $u = [ 'id' => rand(), 'first_name' => 'User' . rand() ];
    $g = [ 'id' => rand(), 'type' => 'group', 'title' => 'Group' . rand() ];
    $sg = [ 'id' => rand(), 'type' => 'supergroup',
      'title' => 'Supergroup' . rand() ];
    $c = [ 'id' => rand(), 'type' => 'channel', 'title' => 'Channel' . rand() ];
    $m = [ 'message_id' => rand(), 'from' => $u, 'chat' => $g, 'date' => time(),
      'text' => 'Test Message: ' . rand() ];
    $cp = [ 'message_id' => rand(), 'chat' => $c, 'date' => time(),
      'text' => 'Test Channel Post: ' . rand() ];
    $iq = [ 'id' => 'inline_' . rand(), 'from' => $u, 'offset' => '',
      'query' => 'Query: ' . rand() ];
    $cir = [ 'result_id' => 'chosen_inline_result_' . rand(), 'from' => $u,
      'query' => 'Query: ' . rand() ];
    $cq = [ 'id' => 'callback_query_' . rand(), 'from' => $u,
      'chat_instance' => 'chat_instance_' . rand() ];
    $l = [
      'longitude' => $this->makeFloat(), 'latitude' => $this->makeFloat() ];
    $uc = [ 'type' => 'private' ] + $u;

    return [
      [ TelegramObjects\Update::class,
        [ 'update_id' => rand(), 'message' => $m ] ],
      [ TelegramObjects\Update::class,
        [ 'update_id' => rand(),
          'edited_message' => [ 'edit_date' => time() ] + $m ] ],
      [ TelegramObjects\Update::class,
        [ 'update_id' => rand(), 'channel_post' => $cp ] ],
      [ TelegramObjects\Update::class,
        [ 'update_id' => rand(),
          'edited_channel_post' => [ 'edit_date' => time() ] + $m ] ],
      [ TelegramObjects\Update::class,
        [ 'update_id' => rand(), 'inline_query' => $iq ] ],
      [ TelegramObjects\Update::class,
        [ 'update_id' => rand(), 'chosen_inline_result' => $cir ] ],
      [ TelegramObjects\Update::class,
        [ 'update_id' => rand(), 'callback_query' => $cq ] ],

      [ TelegramObjects\WebhookInfo::class,
        [ 'url' => 'https://example.com/?' . rand(),
          'has_custom_certificate' => false,
          'pending_update_count' => rand(),
          'last_error_date' => time() - 120,
          'last_error_message' => 'Error: ' . rand(),
          'max_connections' => rand(0, 100),
          'allowed_updates' => [ 'message', 'edited_message' ] ] ],

      [ TelegramObjects\User::class,
        [ 'last_name' => 'Lastname' . rand(),
          'username' => 'username_' . rand() ] + $u ],

      [ TelegramObjects\Chat::class,
        [ 'type' => 'private', 'last_name' => 'Lastname' . rand(),
          'username' => 'username_' . rand() ] + $u ],
      [ TelegramObjects\Chat::class,
        [ 'all_members_are_administrators' => true ] + $g ],
      [ TelegramObjects\Chat::class,
        [ 'username' => 'chan_' . rand() ] + $c ],

      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $c,
          'text' => 'Message Text: ' . rand() ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'forward_from' => [ 'id' => rand(), 'first_name' => 'User'.rand() ],
          'forward_date' => time() - 15, 'text' => 'Forwarded: ' . rand() ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
        'forward_from_chat' => $c, 'forward_from_message_id' => rand(),
        'forward_date' => time() - 30 ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'reply_to_message' => $m,
          'text' => 'Example link',
          'entities' =>
            [ [ 'type' => 'text_link', 'offset' => 7, 'length' => 4,
                'url' => 'https://example.com' ] ] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'audio' =>
            [ 'file_id' => 'file_' . rand(), 'duration' => rand(60, 300) ] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'document' => [ 'file_id' => 'file_' . rand() ] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'sticker' => [ 'file_id' => 'file_' . rand(),
            'width' => rand(256, 512), 'height' => rand(256, 512) ] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'game' =>
          [ 'title' => 'Game ' . rand(), 'description' => '' . rand(),
            'photo' =>
              [ [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 1920),
                  'height' => rand(100, 1080) ] ] ] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'photo' =>
            [ [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 1920),
                'height' => rand(100, 1080) ] ] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'caption' => 'Test Caption: ' . rand(),
          'video' =>
            [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 720),
              'height' => rand(100, 1280), 'duration' => rand(5, 120) ] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'voice' =>
            [ 'file_id' => 'file_' . rand(), 'duration' => rand(5, 60) ] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'contact' =>
            [ 'phone_number' => '+1 285 123 4567',
              'first_name' => 'Contact' . rand() ] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'location' => $l ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'venue' =>
            [ 'location' => $l, 'title' => 'Venue ' . rand(),
              'address' => rand() . ' Infinite Loop' ] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $g,
          'new_chat_member' => $u ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $g,
          'left_chat_member' => $u ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $g,
          'new_chat_title' => 'New Chat Title ' . rand() ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $g,
          'new_chat_photo' =>
            [ [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 512),
                'height' => rand(100, 512) ] ] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $g,
          'delete_chat_photo' => true ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $g,
          'group_chat_created' => true ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $sg,
          'supergroup_chat_created' => true ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $c,
          'channel_chat_created' => true ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $g,
          'migrate_to_chat_id' => $sg['id'] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $sg,
          'migrate_from_chat_id' => $g['id'] ] ],
      [ TelegramObjects\Message::class,
        [ 'message_id' => rand(), 'date' => time(), 'chat' => $uc,
          'pinned_message' => $m ] ],

      [ TelegramObjects\MessageEntity::class,
        [ 'type' => 'mention', 'offset' => rand(0, 10),
          'length' => rand(0, 10) ] ],
      [ TelegramObjects\MessageEntity::class,
        [ 'type' => 'hashtag', 'offset' => rand(0, 10),
          'length' => rand(0, 10) ] ],
      [ TelegramObjects\MessageEntity::class,
        [ 'type' => 'bot_command', 'offset' => rand(0, 10),
          'length' => rand(0, 10) ] ],
      [ TelegramObjects\MessageEntity::class,
        [ 'type' => 'url', 'offset' => rand(0, 10), 'length' => rand(0, 10) ] ],
      [ TelegramObjects\MessageEntity::class,
        [ 'type' => 'email', 'offset' => rand(0, 10),
          'length' => rand(0, 10) ] ],
      [ TelegramObjects\MessageEntity::class,
        [ 'type' => 'bold', 'offset' => rand(0, 10),
          'length' => rand(0, 10) ] ],
      [ TelegramObjects\MessageEntity::class,
        [ 'type' => 'italic', 'offset' => rand(0, 10),
          'length' => rand(0, 10) ] ],
      [ TelegramObjects\MessageEntity::class,
        [ 'type' => 'code', 'offset' => rand(0, 10),
          'length' => rand(0, 10) ] ],
      [ TelegramObjects\MessageEntity::class,
        [ 'type' => 'pre', 'offset' => rand(0, 10), 'length' => rand(0, 10) ] ],
      [ TelegramObjects\MessageEntity::class,
        [ 'type' => 'text_link', 'offset' => rand(0, 10),
          'length' => rand(0, 10), 'url' => 'https://example.com' ] ],
      [ TelegramObjects\MessageEntity::class,
        [ 'type' => 'text_mention', 'offset' => rand(0, 10),
          'length' => rand(0, 10), 'user' => $u ] ],

      [ TelegramObjects\PhotoSize::class,
        [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 1000),
          'height' => rand(100, 1000), 'file_size' => rand() ] ],

      [ TelegramObjects\Audio::class,
        [ 'file_id' => 'file_' . rand(), 'duration' => rand(60, 300),
          'performer' => 'Performer' . rand(), 'title' => 'Title' . rand(),
          'mime_type' => 'audio/mpeg', 'file_size' => rand() ] ],

      [ TelegramObjects\Document::class,
        [ 'file_id' => 'file_' . rand(), 'file_name' => 'Doc' . rand() . '.txt',
          'mime_type' => 'text/plain', 'file_size' => rand(),
          'thumb' => [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 512),
            'height' => rand(100, 512) ] ] ],

      [ TelegramObjects\Sticker::class,
        [ 'file_id' => 'file_' . rand(), 'width' => rand(256, 512),
          'height' => rand(256, 512), 'emoji' => '\u{1F602}',
          'file_size' => rand(),
          'thumb' => [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 512),
            'height' => rand(100, 512) ] ] ],

      [ TelegramObjects\Video::class,
        [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 720),
          'height' => rand(100, 1280), 'duration' => rand(5, 120),
          'mime_type' => 'video/mpeg', 'file_size' => rand(),
          'thumb' => [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 512),
            'height' => rand(100, 512) ] ] ],

      [ TelegramObjects\Voice::class,
        [ 'file_id' => 'file_' . rand(), 'duration' => rand(5, 60),
          'mime_type' => 'audio/mpeg', 'file_size' => rand() ] ],

      [ TelegramObjects\Contact::class,
        [ 'phone_number' => '+1 285 123 4567', 'first_name' => 'Contact'.rand(),
          'last_name' => 'Lastname'.rand(), 'user_id' => rand() ] ],

      [ TelegramObjects\Location::class, $l],

      [ TelegramObjects\Venue::class,
        [ 'location' => $l, 'title' => 'Venue' . rand(),
          'address' => rand() . ' Infinite Loop',
          'foursquare_id' => '4sq' . rand() ] ],

      [ TelegramObjects\UserProfilePhotos::class,
        [ 'total_count' => 3,
          'photos' => [
            [
              [ 'file_id' => 'file_' . rand(), 'width' => rand(),
                'height' => rand() ],
              [ 'file_id' => 'file_' . rand(), 'width' => rand(),
                'height' => rand() ],
              [ 'file_id' => 'file_' . rand(), 'width' => rand(),
                'height' => rand() ],
            ],
            [
              [ 'file_id' => 'file_' . rand(), 'width' => rand(),
                'height' => rand() ],
              [ 'file_id' => 'file_' . rand(), 'width' => rand(),
                'height' => rand() ],
              [ 'file_id' => 'file_' . rand(), 'width' => rand(),
                'height' => rand() ],
            ],
            [
              [ 'file_id' => 'file_' . rand(), 'width' => rand(),
                'height' => rand() ],
              [ 'file_id' => 'file_' . rand(), 'width' => rand(),
                'height' => rand() ],
              [ 'file_id' => 'file_' . rand(), 'width' => rand(),
                'height' => rand() ],
            ] ] ] ],

      [ TelegramObjects\File::class,
        [ 'file_id' => 'file_' . rand(), 'file_size' => rand(),
          'file_path' => 'files/file_' . rand() . '_' . rand() ] ],

      [ TelegramObjects\CallbackQuery::class,
        [ 'id' => 'callback_' . rand(), 'from' => $u,
          'chat_instance' => 'chat_instance_' . rand(),
          'message' => $m,
          'data' => 'inline_data_' . rand() ] ],
      [ TelegramObjects\CallbackQuery::class,
        [ 'id' => 'callback_' . rand(), 'from' => $u,
          'chat_instance' => 'chat_instance_' . rand(),
          'inline_message_id' => 'inline_' . rand(),
          'data' => 'inline_data_' . rand() ] ],
      [ TelegramObjects\CallbackQuery::class,
        [ 'id' => 'callback_' . rand(), 'from' => $u,
          'chat_instance' => 'chat_instance_' . rand(),
          'game_short_name' => 'game_' . rand(),
          'data' => 'inline_data_' . rand() ] ],

      [ TelegramObjects\ChatMember::class,
        [ 'user' => $u, 'status' => 'creator' ] ],
      [ TelegramObjects\ChatMember::class,
        [ 'user' => $u, 'status' => 'administrator' ] ],
      [ TelegramObjects\ChatMember::class,
        [ 'user' => $u, 'status' => 'member' ] ],
      [ TelegramObjects\ChatMember::class,
        [ 'user' => $u, 'status' => 'left' ] ],
      [ TelegramObjects\ChatMember::class,
        [ 'user' => $u, 'status' => 'kicked' ] ],

      [ TelegramObjects\InlineQuery::class,
        [ 'id' => 'inline_' . rand(), 'from' => $u, 'location' => $l,
          'query' => 'Query: ' . rand(), 'offset' => 'offset_' . rand() ] ],

      [ TelegramObjects\ChosenInlineResult::class,
        [ 'result_id' => 'cir_' . rand(), 'from' => $u, 'location' => $l,
          'inline_message_id' => 'inline_' . rand(),
          'query' => 'Query: ' . rand() ] ],

      [ TelegramObjects\Game::class,
        [ 'title' => 'Game ' . rand(), 'description' => '' . rand(),
          'text' => 'Game description: ' . rand(),
          'animation' => [ 'file_id' => 'file_' . rand() ],
          'text_entities' => [ [ 'type' => 'pre', 'offset' => rand(0, 10),
              'length' => rand(0, 10) ] ],
          'photo' => [ [ 'file_id' => 'file_' . rand(),
            'width' => rand(100, 512), 'height' => rand(100, 512) ] ] ] ],

      [ TelegramObjects\Animation::class,
        [ 'file_id' => 'file_' . rand(), 'file_size' => rand(),
          'mime_type' => 'image/gif',
          'file_name' => 'animation_' . rand() . '.gif',
          'thumb' => [ 'file_id' => 'file_' . rand(),
            'width' => rand(100, 512), 'height' => rand(100, 512) ] ] ],

      [ TelegramObjects\GameHighScore::class,
        [ 'position' => rand(100, 300), 'user' => $u, 'score' => rand() ] ],
    ];
  }

  /**
   * @dataProvider objectDataProvider
   */
  public function testObjectInstantiation($class, $json)
  {
    try {
      $o = $class::fromJSON($json);
      $this->assertNotNull($o);
      $this->assertInstanceOf($class, $o);
    } catch (\Exception $e) {
      $this->fail($e);
    }
  }

  /**
   * @dataProvider objectDataProvider
   */
  public function testAbstractObjectFeatures($class, $json)
  {
    $o = $class::fromJSON($json);

    $this->assertEquals(
      sprintf('[%s] %s', get_class($o), json_encode($json, JSON_FORCE_OBJECT)),
      (string) $o
    );
  }

  public function genericArrayDataProvider()
  {
    return [
      [ TelegramObjects\Auxilinary\ChatMemberArray::class,
        [ [ 'status' => 'creator',
            'user' => [ 'id' => rand(), 'first_name' => 'User' . rand() ] ],
          [ 'status' => 'administrator',
            'user' => [ 'id' => rand(), 'first_name' => 'User' . rand() ] ],
          [ 'status' => 'member',
            'user' => [ 'id' => rand(), 'first_name' => 'User' . rand() ] ],
          [ 'status' => 'left',
            'user' => [ 'id' => rand(), 'first_name' => 'User' . rand() ] ],
          [ 'status' => 'kicked',
            'user' => [ 'id' => rand(), 'first_name' => 'User' . rand() ] ] ] ],
      [ TelegramObjects\Auxilinary\GameHighScoreArray::class,
        [ [ 'position' => rand(), 'score' => rand(),
            'user' => [ 'id' => rand(), 'first_name' => 'User' . rand() ] ],
          [ 'position' => rand(), 'score' => rand(),
            'user' => [ 'id' => rand(), 'first_name' => 'User' . rand() ] ],
          [ 'position' => rand(), 'score' => rand(),
            'user' => [ 'id' => rand(), 'first_name' => 'User' . rand() ] ] ] ],
      [ TelegramObjects\Auxilinary\MessageEntityArray::class,
        [ [ 'type' => 'mention', 'offset' => rand(0, 20),
            'length' => rand(0, 20) ],
          [ 'type' => 'hashtag', 'offset' => rand(0, 20),
            'length' => rand(0, 20) ],
          [ 'type' => 'bot_command', 'offset' => rand(0, 20),
            'length' => rand(0, 20) ],
          [ 'type' => 'url', 'offset' => rand(0, 20),
            'length' => rand(0, 20) ],
          [ 'type' => 'email', 'offset' => rand(0, 20),
            'length' => rand(0, 20) ],
          [ 'type' => 'bold', 'offset' => rand(0, 20),
            'length' => rand(0, 20) ],
          [ 'type' => 'italic', 'offset' => rand(0, 20),
            'length' => rand(0, 20) ],
          [ 'type' => 'code', 'offset' => rand(0, 20),
            'length' => rand(0, 20) ],
          [ 'type' => 'pre', 'offset' => rand(0, 20),
            'length' => rand(0, 20) ],
          [ 'type' => 'text_link', 'offset' => rand(0, 20),
            'length' => rand(0, 20), 'url' => 'https://example.com' ],
          [ 'type' => 'text_mention', 'offset' => rand(0, 20),
            'length' => rand(0, 20),
            'user' => [ 'id' => rand(), 'first_name' => 'User' . rand() ] ] ] ],
      [ TelegramObjects\Auxilinary\PhotoSizeArray::class,
        [ [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 1920),
            'height' => rand(100, 1080) ],
          [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 1920),
              'height' => rand(100, 1080) ],
          [ 'file_id' => 'file_' . rand(), 'width' => rand(100, 1920),
              'height' => rand(100, 1080) ] ] ],
      [ TelegramObjects\Auxilinary\UpdateArray::class,
        [ [ 'update_id' => rand(),
            'message' => [ 'message_id' => rand(), 'date' => time(),
              'chat' => [ 'id' => rand(), 'type' => 'private',
                'first_name' => 'User' . rand() ] ] ],
          [ 'update_id' => rand(),
            'edited_message' => [ 'message_id' => rand(), 'date' => time() - 10,
              'chat' => [ 'id' => rand(), 'type' => 'private',
                'first_name' => 'User' . rand() ],
              'edit_date' => time() ] ],
          [ 'update_id' => rand(),
            'channel_post' => [ 'message_id' => rand(), 'date' => time(),
              'chat' => [ 'id' => rand(), 'type' => 'channel',
                'title' => 'Channel' . rand() ] ] ],
          [ 'update_id' => rand(),
            'edited_channel_post' => [ 'message_id' => rand(),
              'date' => time() - 10, 'edit_date' => time(),
              'chat' => [ 'id' => rand(), 'type' => 'channel',
                'title' => 'Channel' . rand() ] ] ] ] ]
    ];
  }

  /**
   * @dataProvider genericArrayDataProvider
   */
  public function testGenericArrayFeatures($array_class, $json)
  {
    $array = $array_class::fromJSON($json);
    $array[] = $array[0];
    $array[0] = $array[1];
    unset($array[0]);

    foreach ($array as $item) {
      $array[] = $item;
    }

    $array[] = $json[0]; // auto typecast

    $this->assertTrue(true); // pass!
  }
}
