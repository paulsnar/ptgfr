<?php

use PHPUnit\Framework\TestCase;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpKernel\KernelEvents;
use Pimple\Container;
use Tgfr\Events\Routing\UpdateEvent;
use Tgfr\Events\Routing\MessageEvent;
use Tgfr\Events\Routing\MessageEditedEvent;
use Tgfr\Events\Routing\ChannelPostEvent;
use Tgfr\Events\Routing\ChannelPostEditedEvent;
use Tgfr\Events\Routing\InlineQueryEvent;
use Tgfr\Events\Routing\ChosenInlineResultEvent;
use Tgfr\Events\Routing\CallbackQueryEvent;
use Tgfr\TelegramObjects\Update;
use Tgfr\TelegramObjects\Message;
use Tgfr\TelegramObjects\InlineQuery;
use Tgfr\Routing\Router;

class RouterTest extends TestCase
{
  protected $dc;
  protected $router;

  public function setUp()
  {
    $this->dc = new Container();
    $this->dc['Bot'] = function () {
      return (object) [ 'self' => 'yes' ];
    };
    $this->dc['EventDispatcher'] = function () {
      return new EventDispatcher();
    };
    $this->dc['config.early_response'] = false;

    $this->router = new Router($this->dc);
  }

  public function tearDown()
  {
    \Mockery::close();
  }

  public function testConvenienceAccessors()
  {
    $this->assertEquals(
      $this->dc['Router.CommandManager'], $this->router->getCommandManager());
    $this->assertEquals(
      $this->dc['Router.HotwordManager'], $this->router->getHotwordManager());
    $this->assertEquals(
      $this->dc['Router.GameManager'], $this->router->getGameManager());
  }

  public function commandRoutingDataProvider()
  {
    return [
      [
        Update::fromJSON([
          'update_id' => rand(),
          'message' => [
            'message_id' => rand(),
            'date' => time(),
            'chat' =>
              [ 'id' => rand(), 'type' => 'private', 'first_name' => 'User' ],
            'text' => '/command',
          ],
        ]),
        [ '/command' ],
      ], [
        Update::fromJSON([
          'update_id' => rand(),
          'message' => [
            'message_id' => rand(),
            'date' => time(),
            'chat' =>
              [ 'id' => rand(), 'type' => 'private', 'first_name' => 'User' ],
            'text' => '/command_with_args arg1 arg2',
          ],
        ]),
        [ '/command_with_args', 'arg1', 'arg2' ],
      ], [
        Update::fromJSON([
          'update_id' => rand(),
          'message' => [
            'message_id' => rand(),
            'date' => time(),
            'chat' =>
              [ 'id' => rand(), 'type' => 'private', 'first_name' => 'User' ],
            'text' => '/command_normalize@bot_username arg1 arg2 arg3',
          ],
        ]),
        [ '/command_normalize', 'arg1', 'arg2', 'arg3' ],
      ]
    ];
  }

  /**
   * @dataProvider commandRoutingDataProvider
   */
  public function testCommandRouting($update, $args)
  {
    $command = array_shift($args);

    $this->dc['Router.CommandManager']->handle(
      $command,
      function ($parsed_cmd, $parsed_args, $msg)
      use ($command, $args, $update) {
        $this->assertEquals($command, $parsed_cmd);
        $this->assertEquals($args, $parsed_args);
        $this->assertEquals($update->getMessage(), $msg);
      }
    );

    $this->dc['EventDispatcher']
      ->dispatch(UpdateEvent::NAME, new UpdateEvent($update));
  }

  public function routingTransformedEventDataProvider()
  {
    return [
      [
        MessageEvent::NAME,
        Update::fromJSON([
          'update_id' => rand(),
          'message' => [
            'message_id' => rand(),
            'date' => time(),
            'chat' => [ 'id' => rand(), 'type' => 'group', 'title' => 'Group' ],
          ],
        ]),
        'getMessage', 'getMessage',
      ], [
        MessageEditedEvent::NAME,
        Update::fromJSON([
          'update_id' => rand(),
          'edited_message' => [
            'message_id' => rand(),
            'date' => time() - 10,
            'edit_date' => time(),
            'chat' => [ 'id' => rand(), 'type' => 'group', 'title' => 'Group' ],
          ],
        ]),
        'getEditedMessage', 'getMessage',
      ], [
        ChannelPostEvent::NAME,
        Update::fromJSON([
          'update_id' => rand(),
          'channel_post' => [
            'message_id' => rand(),
            'date' => time(),
            'chat' =>
            [ 'id' => -rand(), 'type' => 'channel',
              'title' => 'Test Channel']
          ],
        ]),
        'getChannelPost', 'getMessage',
      ], [
        ChannelPostEditedEvent::NAME,
        Update::fromJSON([
          'update_id' => rand(),
          'edited_channel_post' => [
            'message_id' => rand(),
            'date' => time(),
            'chat' =>
            [ 'id' => -rand(), 'type' => 'channel',
              'title' => 'Test Channel']
          ],
        ]),
        'getEditedChannelPost', 'getMessage',
      ], [
        InlineQueryEvent::NAME,
        Update::fromJSON([
          'update_id' => rand(),
          'inline_query' => [
            'id' => 'inline_' . rand(),
            'from' => [ 'id' => rand(), 'first_name' => 'User' ],
            'query' => 'Test query',
            'offset' => '',
          ],
        ]),
        'getInlineQuery', 'getInlineQuery',
      ], [
        ChosenInlineResultEvent::NAME,
        Update::fromJSON([
          'update_id' => rand(),
          'chosen_inline_result' => [
            'result_id' => 'result_' . rand(),
            'from' => [ 'id' => rand(), 'first_name' => 'User' ],
            'query' => 'Test query',
          ],
        ]),
        'getChosenInlineResult', 'getChosenInlineResult',
      ], [
        CallbackQueryEvent::NAME,
        Update::fromJSON([
          'update_id' => rand(),
          'callback_query' => [
            'id' => 'inline_' . rand(),
            'from' => [ 'id' => rand(), 'first_name' => 'User' ],
            'chat_instance' => 'chat_' . rand(),
          ],
        ]),
        'getCallbackQuery', 'getCallbackQuery',
      ]
    ];
  }

  /**
   * @dataProvider routingTransformedEventDataProvider
   */
  public function testTransfromedRoutingEventDispatching(
    $event_name,
    $update,
    $update_method,
    $event_method
  )
  {
    $ed = $this->dc['EventDispatcher'];

    $mock_listener = \Mockery::mock('listener_' . rand(),
      function ($mock) {
        $mock->shouldReceive('handle');
      });

    $ed->addListener($event_name, [ $mock_listener, 'handle' ]);
    $ed->addListener($event_name,
      function ($e) use ($update, $update_method, $event_method) {
        $this->assertEquals(
          $update->$update_method(),
          $e->$event_method());
      });

    $ed->dispatch(UpdateEvent::NAME, new UpdateEvent($update));
  }

  // Wow, I've never seen a language with such scoping issues!
  // PHP essentially binds closured variables to the closures once they're
  // evaluated and later doesn't update the references, at least when using
  // primitive values such as booleans.
  // Therefore we excorporate the state out to here for the callback that should
  // be called after a primitive value reference is updated.
  // Wow PHP, nice moves, keep it up.
  private $_testQueuedUpdateProcessing_allow_pass;

  public function testQueuedUpdateProcessing()
  {
    $u = Update::fromJSON([
      'update_id' => rand(),
      'message' => [
        'message_id' => rand(), 'date' => time(),
        'chat' => [ 'id' => rand(), 'type' => 'private',
          'first_name' => 'User' ],
        'text' => 'Test update!'
      ]
    ]);

    $this->dc['config.early_response'] = true;

    $this->_testQueuedUpdateProcessing_allow_pass = false;

    $mock_listener = function ($event) use ($u) {
      if (!$this->_testQueuedUpdateProcessing_allow_pass) {
        $this->fail('Mock listener was called too early!');
        return false;
      }
      $this->assertEquals($u->getMessage(), $event->getMessage());
    };

    $ed = $this->dc['EventDispatcher'];
    $ed->addListener(MessageEvent::NAME, $mock_listener);
    $ed->dispatch(UpdateEvent::NAME, new UpdateEvent($u));

    $this->_testQueuedUpdateProcessing_allow_pass = true;
    $ed->dispatch(KernelEvents::TERMINATE, null /* who cares */);
  }
}
