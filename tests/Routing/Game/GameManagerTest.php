<?php

use PHPUnit\Framework\TestCase;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Tgfr\Events\Routing\CallbackQueryEvent;
use Tgfr\TelegramObjects\CallbackQuery;
use Tgfr\Routing\Game\GameManager;

class GameManagerTest extends TestCase
{
  use \Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

  protected $dc;
  protected $gm;

  public function setUp()
  {
    $this->dc = new \Pimple\Container();
    $this->dc['Bot'] = function () {
      return (object) [ 'self' => 'yes' ];
    };
    $this->dc['EventDispatcher'] = function () {
      return new EventDispatcher();
    };

    $this->gm = new GameManager($this->dc);
  }

  public function testGameManager()
  {
    $callback_query_1 = CallbackQuery::fromJSON([
      'id' => 'callback_' . rand(), 'chat_instance' => 'chat_' . rand(),
      'from' => [ 'id' => rand(), 'first_name' => 'User' ],
      'game_short_name' => 'game_1',
    ], $this->dc);
    $callback_query_2 = CallbackQuery::fromJSON([
      'id' => 'callback_' . rand(), 'chat_instance' => 'chat_' . rand(),
      'from' => [ 'id' => rand(), 'first_name' => 'User' ],
      'game_short_name' => 'game_2',
    ], $this->dc);

    $url_1 = 'https://example.com/#' . rand();
    $url_2 = 'https://example.com/#' . rand();

    $this->dc['TelegramAPI'] = function ()
    use ($callback_query_1, $callback_query_2, $url_1, $url_2) {
      return \Mockery::mock('Tgfr\\TelegramAPI\\TelegramAPIInterface',
        function ($mock)
        use ($callback_query_1, $callback_query_2, $url_1, $url_2) {
          $mock->shouldReceive('invoke')
            ->with('answerCallbackQuery', [
              'callback_query_id' => $callback_query_1->getId(),
              'url' => $url_1,
            ])
            ->andReturn([ 'ok' => true, 'result' => true ]);

          $mock->shouldReceive('invoke')
            ->with('answerCallbackQuery', [
              'callback_query_id' => $callback_query_2->getId(),
              'url' => $url_2,
            ])
            ->andReturn([ 'ok' => true, 'result' => true ]);
        });
    };

    $mock_listener = \Mockery::mock('CallableListener')
      ->shouldReceive('handle')
      ->with($callback_query_2)
      ->andReturn($url_2)
      ->mock();

    $this->gm->addGame('game_1', $url_1);
    $this->gm->addGame('game_2', [$mock_listener, 'handle']);

    $this->dc['EventDispatcher']->dispatch(CallbackQueryEvent::NAME,
      new CallbackQueryEvent($callback_query_1));
    $this->dc['EventDispatcher']->dispatch(CallbackQueryEvent::NAME,
      new CallbackQueryEvent($callback_query_2));

    $this->assertTrue(true); // pass!
  }

  public function testInvalidGameAddAttempt()
  {
    $this->expectException(\InvalidArgumentException::class);

    $this->gm->addGame('test_game', (object) [ 'not' => [ 'a', 'game' ] ]);
  }
}
