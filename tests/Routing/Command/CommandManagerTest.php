<?php

use PHPUnit\Framework\TestCase;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Tgfr\Events\Routing\CommandEvent;
use Tgfr\TelegramObjects\Message;
use Tgfr\Routing\Command\CommandManager;
use Tgfr\Routing\Command\CommandHandlerInterface;

class CommandManagerTest extends TestCase
{
  use \Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

  protected $dc;
  protected $cm;

  public function setUp()
  {
    $this->dc = new \Pimple\Container();
    $this->dc['Bot'] = function () {
      return (object) [ 'self' => 'yes' ];
    };
    $this->dc['EventDispatcher'] = function () {
      return new EventDispatcher();
    };

    $this->cm = new CommandManager($this->dc);
  }

  public function testCallableHandlers()
  {
    $cmd = '/callable_command';
    $args = ['arg1', 'arg2'];
    $msg = Message::fromJSON([
      'message_id' => rand(),
      'date' => time(),
      'chat' => [ 'id' => rand(), 'type' => 'private', 'first_name' => 'User' ],
      'text' => $cmd . ' ' .implode(' ', $args),
    ], $this->dc);
    $e = new CommandEvent($cmd, $args, $msg);

    $m = \Mockery::mock('CallableHandler',
      function ($mock) use ($cmd, $args, $msg) {
        $mock->shouldReceive('handle')
          ->with($cmd, $args, $msg, $this->dc['Bot']);
      });

    $this->cm->handle($cmd, [$m, 'handle']);
    $this->dc['EventDispatcher']->dispatch(CommandEvent::NAME, $e);

    $this->assertTrue(true); // pass!
  }

  public function testInterfaceHandlers()
  {
    $cmd = '/interface_command';
    $args = ['arg1', 'arg2', 'arg3'];
    $msg = Message::fromJSON([
      'message_id' => rand(),
      'date' => time(),
      'chat' => [ 'id' => rand(), 'type' => 'private', 'first_name' => 'User' ],
      'text' => $cmd . ' ' . implode(' ', $args),
    ], $this->dc);
    $e = new CommandEvent($cmd, $args, $msg);

    $m = \Mockery::mock(CommandHandlerInterface::class,
      function ($mock) use ($cmd, $args, $msg) {
        $mock
          ->shouldReceive('getHandledCommand')
          ->andReturn($cmd);

        $mock
          ->shouldReceive('handleCommand')
          ->with($cmd, $args, $msg);
      });

    $this->cm->handle($m);
    $this->dc['EventDispatcher']->dispatch(CommandEvent::NAME, $e);

    $this->assertTrue(true); // pass!
  }

  public function testInvalidHandlerRejection()
  {
    $this->expectException(\InvalidArgumentException::class);

    $this->cm->handle('/command', (object) [ 'handler' => true ]);
  }
}
