<?php

use PHPUnit\Framework\TestCase;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Tgfr\Events\Routing\TextMessageEvent;
use Tgfr\TelegramObjects\Message;
use Tgfr\Routing\Hotword\HotwordManager;
use Tgfr\Routing\Hotword\HotwordHandlerInterface;

class HotwordManagerTest extends TestCase
{
  use \Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

  protected $dc;
  protected $hm;

  public function setUp()
  {
    $this->dc = new \Pimple\Container();
    $this->dc['Bot'] = function () {
      return (object) [ 'self' => 'yes' ];
    };
    $this->dc['EventDispatcher'] = function () {
      return new EventDispatcher();
    };

    $this->hm = new HotwordManager($this->dc);
  }

  public function testHotwordDispatch()
  {
    $msg = Message::fromJSON([
      'message_id' => rand(), 'date' => time(),
      'chat' => [ 'id' => rand(), 'type' => 'private', 'first_name' => 'User' ],
      'text' => 'hot word',
    ]);
    $e = new TextMessageEvent($msg);

    $m = \Mockery::mock('CallableHandler',
      function ($mock) use ($msg) {
        $mock->shouldReceive('handle1')
          ->with('hot', $msg, $this->dc['Bot']);

        $mock->shouldReceive('handle2')
          ->with('/word/i', $msg, $this->dc['Bot']);
      });

    $this->hm->addHotword('hot', [$m, 'handle1']);
    $this->hm->addHotword('/word/i', [$m, 'handle2'], true);
    $this->dc['EventDispatcher']->dispatch(TextMessageEvent::NAME, $e);

    $this->assertTrue(true); // pass!
  }

  public function testHotwordDispatchInterruption()
  {
    $msg_1 = Message::fromJSON([
      'message_id' => rand(), 'date' => time(),
      'chat' => [ 'id' => rand(), 'type' => 'private', 'first_name' => 'User' ],
      'text' => 'stop a hot word'
    ]);
    $e_1 = new TextMessageEvent($msg_1);

    $msg_2 = Message::fromJSON([
      'message_id' => rand(), 'date' => time(),
      'chat' => [ 'id' => rand(), 'type' => 'private', 'first_name' => 'User' ],
      'text' => 'stop a regex handler'
    ]);
    $e_2 = new TextMessageEvent($msg_2);

    $m = \Mockery::mock('CallableHandler',
      function ($mock) use ($msg_1, $msg_2) {
        $mock->shouldReceive('handle1')
          ->with('hot word', $msg_1, $this->dc['Bot'])
          ->andReturn(true);

        $mock->shouldReceive('handle2')
          ->with('/regex/i', $msg_2, $this->dc['Bot'])
          ->andReturn(true);
      });
    $m2 = \Mockery::mock('CallableIgnorableHandler');

    $this->hm->addHotword('hot word', [$m, 'handle1']);
    $this->hm->addHotword('hot word', [$m2, 'handle1']);
    $this->dc['EventDispatcher']->dispatch(TextMessageEvent::NAME, $e_1);

    $this->hm->addHotword('/regex/i', [$m, 'handle2'], true);
    $this->hm->addHotword('/regex/i', [$m2, 'handle2'], true);
    $this->dc['EventDispatcher']->dispatch(TextMessageEvent::NAME, $e_2);

    $this->assertTrue(true); // pass!
  }

  public function testInterfaceHandlers()
  {
    $msg = Message::fromJSON([
      'message_id' => rand(), 'date' => time(),
      'chat' => [ 'id' => rand(), 'type' => 'private', 'first_name' => 'User' ],
      'text' => 'extremely hot word',
    ], $this->dc);
    $e = new TextMessageEvent($msg);

    $m = \Mockery::mock(HotwordHandlerInterface::class,
      function ($mock) use ($msg) {
        $mock
          ->shouldReceive('getHandledHotwords')
          ->andReturn(['hot']);

        $mock
          ->shouldReceive('getHandledRegexes')
          ->andReturn(['/extremely/i']);

        $mock
          ->shouldReceive('handle')
          ->with('hot', $msg);

        $mock
          ->shouldReceive('handle')
          ->with('/extremely/i', $msg);
      });

    $this->hm->addHotword($m);
    $this->dc['EventDispatcher']->dispatch(TextMessageEvent::NAME, $e);

    $this->assertTrue(true); // pass!
  }


  public function testInvalidHandlerRejection()
  {
    $this->expectException(\InvalidArgumentException::class);

    $this->hm->addHotword('very very hot', (object) [ 'handler' => true ]);
  }
}
