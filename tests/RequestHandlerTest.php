<?php

use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tgfr\RequestHandler;

class RequestHandlerTest extends TestCase
{
  protected $dc;
  protected $handler;

  public function setUp()
  {
    $this->dc = new \Pimple\Container();
    $this->dc['config.key'] = null;

    $this->handler = new RequestHandler($this->dc);
  }

  public function tearDown()
  {
    \Mockery::close();
  }

  public function testKeyRequirement()
  {
    $key = '' . rand();
    $this->dc['config.key'] = $key;

    $req = new Request([ ]);
    $resp = $this->handler->__invoke($req);
    $this->assertEquals(Response::HTTP_UNAUTHORIZED, $resp->getStatusCode());

    $req = new Request([ 'key' => 'invalid' ]);
    $resp = $this->handler->__invoke($req);
    $this->assertEquals(Response::HTTP_FORBIDDEN, $resp->getStatusCode());

    $req = new Request([ 'key' => $key ]);
    $resp = $this->handler->__invoke($req);
    $this->assertNotContains(
      $resp->getStatusCode(),
      [ Response::HTTP_UNAUTHORIZED, Response::HTTP_FORBIDDEN ]);
  }

  public function testInvalidJson()
  {
    $req = new Request([ ], [ ], [ ], [ ], [ ],
      [ 'HTTP_CONTENT_TYPE' => 'application/json' ],
      '{"malformed":"json"');
    $resp = $this->handler->__invoke($req);

    $this->assertEquals(Response::HTTP_BAD_REQUEST, $resp->getStatusCode());
    $this->assertContains('JSON', $resp->getContent());
  }

  public function testEmptyRequest()
  {
    $req = new Request();
    $resp = $this->handler->__invoke($req);

    $this->assertEquals(Response::HTTP_BAD_REQUEST, $resp->getStatusCode());
    $this->assertContains('body', $resp->getContent());
  }

  public function testInvalidRequestFormat()
  {
    $req = new Request([ ], [ ], [ ], [ ], [ ],
      [ 'HTTP_CONTENT_TYPE' => 'text/plain' ],
      'plain text request');
    $resp = $this->handler->__invoke($req);

    $this->assertEquals(Response::HTTP_UNSUPPORTED_MEDIA_TYPE,
      $resp->getStatusCode());
    $this->assertContains('format', $resp->getContent());
  }
}
